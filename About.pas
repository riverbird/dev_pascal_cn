unit About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs, marsCap, ShellAPI, Version, Menus, ClipBrd, EggUnit;

type
  TAboutBox = class(TForm)
    AppIcon: TImage;
    Label1: TLabel;
    BuildDate: TLabel;
    Version: TLabel;
    Bevel1: TBevel;
    Memo1: TMemo;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FAQLabelText: TLabel;
    Label8: TLabel;
    CheckForUpdate: TBitBtn;
    CloseBtn: TBitBtn;
    Authors: TBitBtn;
    UrlMenu: TPopupMenu;
    Copy1: TMenuItem;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    FAQLabel: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EggMenu: TPopupMenu;
    Letstheeggbegin1: TMenuItem;
    Egg: TImage;
    CompilerLabel: TLabel;
    GDBLabel: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label7: TLabel;
    Label14: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Image: TImage;
    Image1: TImage;
    Label3: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    procedure AuthorsClick(Sender: TObject);
    procedure CheckForUpdateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure LabelClick(Sender: TObject);
    procedure LabelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AuthorsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure AuthorsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Letstheeggbegin1Click(Sender: TObject);
    procedure Label13Click(Sender: TObject);
    procedure FAQLabelClick(Sender: TObject);
  private
    UrlString   : string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation
uses Main;
{$R *.DFM}

procedure TAboutBox.AuthorsClick(Sender: TObject);
const MessageText =
  'Development:'+#13#10+#13#10+
  'Colin Laplace: Main development.'+ #13#10 +
  'Hongli Lai: Templates, IDE updates.'+ #13#10 +
  'Prof Abimbola Olowofoyeku: IDE support for GNU Pascal.'+ #13#10 +
  'Mumit Khan, Jan-Jaap van der Heijden, Colin Hendrix, Earnie Boyd, Danny Smith and GNU coders: Mingw32 compiler system.'+ #13#10#13#10+
  'Thanks to each component author and those who have contributed to Dev-Pascal (beta testers and people who gave me many good ideas ;-).'+#13#10+#13#10+
  'Special thanks to all the GNU coders, who created the great gcc compiler system,'+#13#10+
  'especially Mumit Khan, Earnie Boyd and Danny Smith for keeping the Mingw32 compiler updated.';
begin
  MessageBeep($F);
  MessageDlg(MessageText, MtInformation, [MbOK], 0);
end;

procedure TAboutBox.CheckForUpdateClick(Sender: TObject);
begin
  MainForm.CheckforDevPasUpdatesItemClick(sender);
end;

procedure TAboutBox.FormCreate(Sender: TObject);
begin
  BuildDate.Caption   := ' Built on: ' + GetCompiledDate;
  Version.Caption := 'v' + DevPasVersion + CompilerString;
  CompilerLabel.Caption := CompilerVersion;
  GDBLabel.Caption := GDBVersion;
  AppIcon.Picture.Icon.Handle := Application.Icon.Handle;
  FAQLabel.Left := FAQLabelText.Left+FAQLabelText.Width-1;
end;

procedure TAboutBox.CloseBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TAboutBox.Copy1Click(Sender: TObject);
begin
  ClipBoard.AsText := UrlString;
end;

procedure TAboutBox.LabelClick(Sender: TObject);
var s : string;
begin
if pos('@',(Sender as TLabel).Caption)<> 0 then
   s := 'mailto:'+(Sender as TLabel).Caption
else s := (Sender as TLabel).Caption;

ShellExecute(GetDesktopWindow(), 'open', PChar(s), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutBox.LabelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Button = mbRight then
   UrlString := (sender as TLabel).Caption;
end;

procedure TAboutBox.AuthorsDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := True
end;

procedure TAboutBox.AuthorsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  Authors.PopupMenu := EggMenu;
  Authors.NumGlyphs := 1;
  Authors.Glyph.Assign(Egg.Picture);
  Authors.ShowHint := True;
  Authors.Hint := 'You have nearly found the egg, do you know what a popup menu is ?';
end;

procedure TAboutBox.Letstheeggbegin1Click(Sender: TObject);
begin
  if Application.MessageBox('Hi, i''m the Egg. Is it easter yet?', 'Hi!',
    MB_ICONQUESTION + MB_YESNO) = 6 {Yes} then
  begin
     Close;
     with TEggForm.Create(self) do
        try
          ShowModal;
        except
          Free;
        end;
  end else
     ShowMessage('Bad answer ;-)');
end;

procedure TAboutBox.Label13Click(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\DevPas.hlp';
  Application.HelpJump('MailingList');
end;

procedure TAboutBox.FAQLabelClick(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\DevPas.hlp';
  Application.HelpJump('FaqcommonProblems');
end;

end.


