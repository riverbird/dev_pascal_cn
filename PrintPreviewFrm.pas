unit PrintPreviewFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SynEditPrintPreview, TB97Ctls, TB97Tlbr, TB97;

type
  TPrintPreviewForm = class(TForm)
    Toolbar: TToolbar97;
    Dock971: TDock97;
    LastBtn: TToolbarButton97;
    NextBtn: TToolbarButton97;
    FirstBtn: TToolbarButton97;
    BackBtn: TToolbarButton97;
    ToolbarSep971: TToolbarSep97;
    PrintPreview: TSynEditPrintPreview;
    ZoomInBtn: TToolbarButton97;
    ZoomOutBtn: TToolbarButton97;
    ToolbarSep973: TToolbarSep97;
    CloseBtn: TToolbarButton97;
    procedure FirstBtnClick(Sender: TObject);
    procedure BackBtnClick(Sender: TObject);
    procedure NextBtnClick(Sender: TObject);
    procedure LastBtnClick(Sender: TObject);
    procedure ZoomOutBtnClick(Sender: TObject);
    procedure ZoomInBtnClick(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PrintPreviewForm: TPrintPreviewForm;

implementation

{$R *.DFM}

procedure TPrintPreviewForm.FirstBtnClick(Sender: TObject);
begin
  PrintPreview.FirstPage;
end;

procedure TPrintPreviewForm.BackBtnClick(Sender: TObject);
begin
  PrintPreview.PreviousPage;
end;

procedure TPrintPreviewForm.NextBtnClick(Sender: TObject);
begin
  PrintPreview.NextPage;
end;

procedure TPrintPreviewForm.LastBtnClick(Sender: TObject);
begin
  PrintPreview.LastPage;
end;

procedure TPrintPreviewForm.ZoomOutBtnClick(Sender: TObject);
begin
  PrintPreview.ScalePercent := PrintPreview.ScalePercent - 50; 
end;

procedure TPrintPreviewForm.ZoomInBtnClick(Sender: TObject);
begin
  PrintPreview.ScalePercent := PrintPreview.ScalePercent + 50;
end;

procedure TPrintPreviewForm.CloseBtnClick(Sender: TObject);
begin
  Close;
end;

end.
