// Compile with Free Pascal compiler or Turbo Pascal 7

program DosCom;

uses Dos;

var
  Command: string[127];
begin
    repeat
    Write('Enter DOS command: ');
    ReadLn(Command);
    if Command <> '' then
    begin
      SwapVectors;
      Exec(GetEnv('COMSPEC'), '/C ' + Command);
      SwapVectors;
      if DosError <> 0 then
        WriteLn('Could not execute COMMAND.COM');
      WriteLn;
    end;
  until (Command = '') or (Command = 'exit');
end.
