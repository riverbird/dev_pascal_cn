�
 TCREATEDIALOGFORM 0  TPF0TCreateDialogFormCreateDialogFormLeftrTop� BorderIconsbiSystemMenu BorderStylebsSingleCaptionCreate new dialogClientHeightUClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInchx
TextHeight 	TGroupBox	GroupBox1Left
Top
WidthwHeightCaptionStandard options :TabOrder  TLabelLabel4LeftTopWidthVHeightCaption;ID of the dialog  (ID can be any number not already used) :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel1LeftTopYWidth� HeightCaptionTitle of the dialog window :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTop� WidthmHeightCaptionDefault font name :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3LeftTop� WidthcHeightCaptionDefault font size :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditIDLeftTop1WidthZHeightTabOrder Text100  TEditTitleLeftToplWidthFHeightTabOrderTextDialog title  TEditFontNameLeftTop� WidthFHeightTabOrderTextHelv  	TSpinEditFontSizeLeftTop� Width]HeightMaxValue MinValue TabOrderValue   	TGroupBox	GroupBox2Left�Top
WidthwHeighttCaptionDialog size and position :TabOrder TLabelLabel5LeftTop%WidthHeightCaptionLeft :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6LeftTopLWidthHeightCaptionTop :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7Left� Top%Width-HeightCaptionHeight :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8Left� TopLWidth(HeightCaptionWidth :Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TSpinEdit	LeftValueLeft;TopWidthdHeightMaxValue MinValue TabOrder Value   	TSpinEditTopValueLeft;TopEWidthdHeightMaxValue MinValue TabOrderValue   	TSpinEditHeightValueLeft� TopWidthdHeightMaxValue MinValue TabOrderValue�   	TSpinEdit
WidthValueLeft� TopEWidthdHeightMaxValue MinValue TabOrderValue�    	TGroupBox	GroupBox3Left�Top� Width� Height� CaptionOptionals :TabOrder 	TCheckBoxSysMenuLeftTopWidthwHeightCaption&System menuChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder   	TCheckBoxMinLeftTop1WidthwHeightCaption&Minimize buttonFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBoxMaxiLeftTopEWidthwHeightCaptionM&aximize buttonFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBox
VertScrollLeftTopYWidthwHeightCaption&Vertical scrollFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBoxHorizScrollLeftToplWidthwHeightCaption&Horizontal scrollFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBoxPopupLeftTop� WidthwHeightCaption&PopupChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder   	TGroupBox	GroupBox4LeftOTop� Width� Height� CaptionFrame style :TabOrder 	TCheckBoxTitleBarLeftTopWidthwHeightCaption
&Title barChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder   	TCheckBoxModalLeftTop1WidthwHeightCaptionM&odalChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder  	TCheckBox	ResizableLeftTopEWidthwHeightCaption
&ResizableFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBox
ClientEdgeLeftTopYWidthwHeightCaption&Client edgeFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBox
StaticEdgeLeftToplWidthwHeightCaptionS&tatic edgeFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TBitBtn	CancelBtnLeft�Top.Width\HeightCaption&CancelTabOrderKindbkCancel  TBitBtnOKBtnLeftBTop.Width]HeightCaption&OKTabOrderKindbkOK   