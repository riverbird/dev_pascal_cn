{
*******************************************************************
*******************************************************************
**                      Dev-Pascal                               **
**                                                               **
** DevPas.dpr: Dev-Pascal Project file.                          **
**                                                               **
** Copyright Bloodshed Software.                                 **
** UNDER THE GNU GENERAL PUBLIC LICENSE. See Copying.txt before  **
** modifiyng or distributing the sources and binaries of         **
** Dev-Pascal                                                    **
**                                                               **
** Please send your modification to dev@bloodshed.net            **
**                                                               **
** Colin Laplace                                                 **
** email: webmaster@bloodshed.net                                **
** http://www.bloodshed.net/                                     **
*******************************************************************
*******************************************************************
}
program DevPas;

{%File 'Makefile.inc'}
{%File 'TemplateFuncs.inc'}

{DEFINE CD_VERSION}
{$DEFINE FULL_VERSION}
uses
  Forms,
  Main in 'Main.pas' {MainForm},
  MDIEdit in 'MDIEdit.pas' {EditForm},
  CompOptions in 'CompOptions.pas' {CompForm},
  About in 'About.pas' {AboutBox},
  EnvirOpt in 'EnvirOpt.pas' {EnvirForm},
  Line in 'Line.pas' {LineForm},
  DebugFrm in 'DebugFrm.pas' {DbgForm},
  Splash in 'Splash.pas' {SplashForm},
  Project in 'Project.pas',
  ProjectOptions in 'ProjectOptions.pas' {ProjectOpt},
  Utils in 'Utils.pas',
  CheckFrm in 'CheckFrm.pas' {CheckForm},
  InfoFrm in 'InfoFrm.pas' {InfoForm},
  OutputFrm in 'OutputFrm.pas' {OutputForm},
  MsgBox in 'MsgBox.pas' {MessageBoxForm},
  NewProject in 'NewProject.pas' {NewProjForm},
  Version in 'Version.pas',
  IconFrm in 'IconFrm.pas' {IconForm},
  DllDialog in 'DllDialog.pas' {DllForm},
  UResCodeEdit in 'UResCodeEdit.pas' {ResCodeEditForm},
  ResEdit in 'ResEdit.pas' {ResEditForm},
  UVerInfoResForm in 'UVerInfoResForm.pas' {VerInfoResForm},
  PrinterFrm in 'PrinterFrm.pas' {Printer},
  EggUnit in 'EggUnit.pas' {EggForm},
  UMakefileForm in 'UMakefileForm.pas' {MakefileForm},
  RemoveUnit in 'RemoveUnit.pas' {RemoveUnitForm},
  MenuFrm in 'MenuFrm.pas' {MenuForm},
  SetupCreator in 'SetupCreator.pas' {SetupCreatorWindow},
  SetupCreatorInfo in 'SetupCreatorInfo.pas' {SetupCreatorInfoWindow},
  TemplateFrm in 'TemplateFrm.pas' {TemplateForm},
  PrintPreviewFrm in 'PrintPreviewFrm.pas' {PrintPreviewForm},
  SetupSelectDir in 'SetupSelectDir.pas' {SelectDirForm},
  ToolFrm in 'ToolFrm.pas' {ToolForm},
  ToolEditFrm in 'ToolEditFrm.pas' {ToolEditForm},
  FirstFrm in 'FirstFrm.pas' {FirstForm},
  CreateDialogFrm in 'CreateDialogFrm.pas' {CreateDialogForm},
  SetMainUnit in 'SetMainUnit.pas' {MainUnitForm};

{$R DevPas.RES}
{$R icons.res}

begin
  SplashForm := TSplashForm.Create(Application);
  SplashForm.Show;
  SplashForm.Update;
  Application.Title := 'Dev-Pascal���İ�';
  application.Icon.LoadFromFile('hudie.ico');
  Application.HelpFile := 'Help\devpas.hlp';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TInfoForm, InfoForm);
  Application.CreateForm(TOutputForm, OutputForm);
  Application.CreateForm(TDbgForm, DbgForm);
  Application.CreateForm(TIconForm, IconForm);
  Application.CreateForm(TCreateDialogForm, CreateDialogForm);
  SplashForm.Free;
  Application.Run;
end.
