unit UMakefileForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, Buttons, StdCtrls, marsCap;

type
  TMakefileForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CompilerName: TEdit;
    SpeedButton1: TSpeedButton;
    PopupMenu1: TPopupMenu;
    StandardCcompilercc1: TMenuItem;
    GNUCCompilergcc1: TMenuItem;
    BorlandCCCompilerbcc1: TMenuItem;
    Label2: TLabel;
    CFlags: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GNUCCompilerg1: TMenuItem;
    procedure StandardCcompilercc1Click(Sender: TObject);
    procedure GNUCCompilergcc1Click(Sender: TObject);
    procedure BorlandCCCompilerbcc1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure GNUCCompilerg1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MakefileForm: TMakefileForm;

implementation

{$R *.DFM}

procedure TMakefileForm.StandardCcompilercc1Click(Sender: TObject);
begin
  CompilerName.Text := 'cc';
end;

procedure TMakefileForm.GNUCCompilergcc1Click(Sender: TObject);
begin
  CompilerName.Text := 'gcc';
end;

procedure TMakefileForm.BorlandCCCompilerbcc1Click(Sender: TObject);
begin
  CompilerName.Text := 'bcc';
end;

procedure TMakefileForm.SpeedButton1Click(Sender: TObject);
var
  P: TPoint;
begin
  P.X := SpeedButton1.Left + GroupBox1.Left - 1;
  P.Y := SpeedButton1.Top + GroupBox1.Top + SpeedButton1.Height;
  P := ClientToScreen(P);
  PopupMenu1.Popup(P.x, P.y);
end;

procedure TMakefileForm.GNUCCompilerg1Click(Sender: TObject);
begin
  CompilerName.Text := 'g++';
end;

end.
