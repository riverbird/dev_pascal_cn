unit SetupCreatorInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, IniFiles, ExtCtrls;

type
  TSetupCreatorInfoWindow = class(TForm)
    Label1: TLabel;
    FileName: TEdit;
    BrowseBtn: TButton;
    OpenDialog: TOpenDialog;
    ok: TBitBtn;
    cancel: TBitBtn;
    Bevel1: TBevel;
    procedure cancelClick(Sender: TObject);
    procedure BrowseBtnClick(Sender: TObject);
    procedure okClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SetupCreatorInfoWindow: TSetupCreatorInfoWindow;

implementation

uses SetupCreator;

{$R *.DFM}

procedure TSetupCreatorInfoWindow.cancelClick(Sender: TObject);
begin
 Close;
end;

procedure TSetupCreatorInfoWindow.BrowseBtnClick(Sender: TObject);
begin
 if OpenDialog.Execute then
    FileName.Text :=OpenDialog.FileName;
end;

procedure TSetupCreatorInfoWindow.okClick(Sender: TObject);
var
 IniFile: TIniFile;
 SrcExePath: String;
begin
 if FileName.Text <> '' then
 begin
  IniFile := TIniFile.Create(FileName.Text);
  SetupCreatorWindow.AppTitle.Text:=IniFile.ReadString('Project','Name','');
  SetupCreatorWindow.AppExe.Text:=IniFile.ReadString('Project','FileName','');
  SrcExePath := ChangeFileExt(SetupCreatorWindow.AppExe.Text, '.exe');
  SetupCreatorWindow.AppExe.Text:=SrcExePath;
 end;

 Close;
end;

end.
