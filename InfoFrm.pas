unit InfoFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, marsCap;

type
  TInfoForm = class(TForm)
    GroupBox: TGroupBox;
    Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
    Bevel6: TBevel;
    Bevel2: TBevel;
    OkBtn: TBitBtn;
    ShowResultsBtn: TBitBtn;
    Label3: TLabel;
    Bevel3: TBevel;
    Label4: TLabel;
    ExecBtn: TBitBtn;
    ProjName: TLabel;
    TotalErrors: TLabel;
    SizeFile: TLabel;
    RunParams: TEdit;
    ExeLabel: TLabel;
    MoreBtn: TBitBtn;
    procedure OkBtnClick(Sender: TObject);
    procedure SeeAllOutputClick(Sender: TObject);
    procedure ExecBtnClick(Sender: TObject);
    procedure MoreBtnClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    NewHeight : integer;
    { Déclarations publiques }
  end;

var
  InfoForm: TInfoForm;

implementation

uses Main;

{$R *.DFM}

procedure TInfoForm.OkBtnClick(Sender: TObject);
begin
  FormStyle := fsNormal;
  Close;
end;

procedure TInfoForm.SeeAllOutputClick(Sender: TObject);
begin
  Close;
  MainForm.ShowAllOutput;
end;

procedure TInfoForm.ExecBtnClick(Sender: TObject);
begin
  MainForm.Execute(RunParams.Text);
end;

procedure TInfoForm.MoreBtnClick(Sender: TObject);
begin
  if ExeLabel.Visible then begin
     Height := NewHeight;
     ExeLabel.Visible := False;
     RunParams.Visible := False;
  end
  else begin
     Height := NewHeight+4+ExeLabel.Height+2+RunParams.Height+4;
     ExeLabel.Visible := True;
     RunParams.Visible := True;
  end;
end;

procedure TInfoForm.FormDeactivate(Sender: TObject);
begin
  Height := NewHeight;
  ExeLabel.Visible := False;
  RunParams.Visible := False;
end;

procedure TInfoForm.FormCreate(Sender: TObject);
begin
  NewHeight := Height;
end;

end.
