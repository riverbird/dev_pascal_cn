unit CompOptions;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Inifiles, ExtCtrls, ComCtrls, marsCap;

type
  TCompForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    PageControl: TPageControl;
    PasTab: TTabSheet;
    Panel1: TPanel;
    CodeGenTab: TTabSheet;
    LinkerTab: TTabSheet;
    DirTab: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    DebugInfo: TCheckBox;
    Panel6: TPanel;
    AddDir: TCheckBox;
    AddDirEdit: TEdit;
    ComCheck: TCheckBox;
    Commands: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    eBinDir: TEdit;
    ePasdir: TEdit;
    eLibDir: TEdit;
    GroupBox1: TGroupBox;
    DelphiExt: TCheckBox;
    COp: TCheckBox;
    LabelGoto: TCheckBox;
    GroupBox4: TGroupBox;
    IOCheck: TCheckBox;
    StackCheck: TCheckBox;
    Overflow: TCheckBox;
    Panel2: TPanel;
    Label6: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    GroupBox5: TGroupBox;
    LinkDyn: TCheckBox;
    HelpBtn: TBitBtn;
    Assert: TCheckBox;
    DelphiComp: TCheckBox;
    Ansi: TCheckBox;
    CInline: TCheckBox;
    TP7: TCheckBox;
    Macros: TCheckBox;
    LinkSmart: TCheckBox;
    StripSymbols: TCheckBox;
    NoReloc: TCheckBox;
    NoCheckUnit: TCheckBox;
    GUIApp: TCheckBox;
    ThisCompilerRadioGroup: TRadioGroup;
    CPURadioGroup: TRadioGroup;
    GroupBox2: TGroupBox;
    SmallCode: TRadioButton;
    QuickOpt: TRadioButton;
    Best: TRadioButton;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure ThisCompilerRadioGroupClick(Sender: TObject);
  private
  IniFileName : string;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  CompForm: TCompForm;
  NumOpt : integer;

implementation

uses Version, Main;
{$R *.DFM}


Procedure DoInit (TheComp : Compiler_Support; ReInit : Boolean);
Begin
  With CompForm
  do begin
      IOCheck.Checked := False;
      StackCheck.Checked := False;
      Overflow.Checked := False;
      DebugInfo.Checked := False;
      NoReloc.Checked := False;
      Assert.Checked := False;
      Ansi.Checked := false;
      COp.Checked := False;
      CInline.Checked := False;
      LinkDyn.Checked := False;
      StripSymbols.Checked := False;
      LinkSmart.Checked := False;

      If Reinit
      then begin
           SmallCode.Checked := False;
           QuickOpt.Checked := True;
           Best.Checked := False;

           AddDir.Checked := False;
           AddDirEdit.Text := '';
           ComCheck.Checked := False;
           Commands.Text := '';
      end;

      Case TheComp of
        GPC :
        begin
           Ansi.Caption := 'Force ISO-&7185 Pascal';
           Macros.Checked := True;

           DelphiExt.Checked := False;
           TP7.Checked := False;
           LabelGoto.Checked := False;
           DelphiComp.Checked := False;
           NoCheckUnit.Checked := False;

           NoReloc.Enabled := False;
           LinkDyn.Enabled := False;
           LinkSmart.Enabled := False;
           NoCheckUnit.Enabled := False;
           COp.Enabled := False;
           TP7.Enabled := False;
           CInline.Enabled := False;
           DelphiComp.Enabled := False;
           DelphiExt.Enabled := False;
           LabelGoto.Enabled := False;
           Assert.Enabled := False;
        end;

        FreePascal :
        begin
           Ansi.Caption := '&Use ansistrings';
           Macros.Checked := False;

           DelphiExt.Checked := True;
           TP7.Checked := True;
           LabelGoto.Checked := True;
           DelphiComp.Checked := True;
           NoCheckUnit.Checked := True;

           NoReloc.Enabled := True;
           LinkDyn.Enabled := True;
           LinkSmart.Enabled := True;
           NoCheckUnit.Enabled := True;
           COp.Enabled := True;
           TP7.Enabled := True;
           CInline.Enabled := True;
           DelphiComp.Enabled := True;
           DelphiExt.Enabled := True;
           LabelGoto.Enabled := True;
           Assert.Enabled := True;
        end;

      end; //case

  end; // With CompForm
End;

procedure TCompForm.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TCompForm.BitBtn1Click(Sender: TObject);
var
IniFile : TIniFile;
begin

  CompilerOptionsModified := True;

  if AddDir.Checked and (AddDirEdit.Text = '') then
     begin
          MessageDlg('You have selected the "Add directory" option but no path have been typed in the field below.', MtError,[MbOK],0);
          AddDir.Checked := False;
     end;

  if ComCheck.Checked and (Commands.Text = '') then
     begin
          MessageDlg('You have selected the "Add commands" option but no commands have been typed in the field below.', MtError,[MbOK],0);
          ComCheck.Checked := False;
     end;

  if (eBinDir.Text = '') or (eLibDir.Text = '') then begin
     MessageDlg('The Bin or Lib directory is empty, press the Default button to set it.', mtWarning, [mbOK], 0);
     ModalResult := mrNone;
     exit;
  end;

  eBinDir.Text := AddBackSlash (eBinDir.Text);
  eLibDir.Text := AddBackSlash (eLibDir.Text);
  
  IniFileName := MainForm.DevPasPath + ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);

  { Write to the ini file the user's desired compiler options }
  IniFile.WriteBool('Pascal_Compiler','DelphiExt',DelphiExt.Checked);
  IniFile.WriteBool('Pascal_Compiler','Assert',Assert.Checked);
  IniFile.WriteBool('Pascal_Compiler','Ansi',Ansi.Checked);
  IniFile.WriteBool('Pascal_Compiler','TP7',TP7.Checked);
  IniFile.WriteBool('Pascal_Compiler','COp',COp.Checked);
  IniFile.WriteBool('Pascal_Compiler','LabelGoto',LabelGoto.Checked);
  IniFile.WriteBool('Pascal_Compiler','DelphiComp',DelphiComp.Checked);
  IniFile.WriteBool('Pascal_Compiler','CInline',CInline.Checked);
  IniFile.WriteBool('Pascal_Compiler','Macros',Macros.Checked);
  IniFile.WriteBool('Pascal_Compiler','LinkDyn',LinkDyn.Checked);
  IniFile.WriteBool('Pascal_Compiler','LinkSmart',LinkSmart.Checked);
  IniFile.WriteBool('Pascal_Compiler','StripSymbols',StripSymbols.Checked);
  IniFile.WriteBool('Pascal_Compiler','NoCheckUnit',NoCheckUnit.Checked);

  IniFile.WriteBool('Code_Generation','IOCheck',IOCheck.Checked);
  IniFile.WriteBool('Code_Generation','StackCheck',StackCheck.Checked);
  IniFile.WriteBool('Code_Generation','Overflow',Overflow.Checked);
  IniFile.WriteBool('Code_Generation','GUI',GUIApp.Checked);

  IniFile.WriteInteger('Code_Generation','Target_Cpu', CPURadioGroup.ItemIndex);

  IniFile.WriteBool('Optimization','SmallCode',SmallCode.Checked);
  IniFile.WriteBool('Optimization','QuickOpt',QuickOpt.Checked);
  IniFile.WriteBool('Optimization','Best',Best.Checked);

  IniFile.WriteBool('Linker','DebugInfo',DebugInfo.Checked);
  IniFile.WriteBool('Linker','NoReloc',NoReloc.Checked);

  if AddDir.Checked then
     begin
       IniFile.WriteBool('Directories','AddDir',AddDir.Checked);
       IniFile.WriteString('Directories','Dir',AddDirEdit.Text);
     end
  else
     begin
       IniFile.WriteBool('Directories','AddDir',False);
       IniFile.WriteString('Directories','Dir','');
     end;

  if ComCheck.Checked then
     begin
       IniFile.WriteBool('Directories','AddCommands',ComCheck.Checked);
       IniFile.WriteString('Directories','Commands',Commands.Text);
     end
  else
     begin
       IniFile.WriteBool('Directories','AddCommands',False);
       IniFile.WriteString('Directories','Commands','');
     end;

  IniFile.WriteString('Directories','BinDir',eBinDir.Text);
  IniFile.WriteString('Directories','PasDir',ePasDir.Text);
  IniFile.WriteString('Directories','LibDir',eLibDir.Text);

  IniFile.WriteInteger('Compiler','CompilerName', ThisCompilerRadioGroup.ItemIndex);
  SetupCompiler;
  Mainform.Caption := ThisCaption;

  with MainForm
  do begin
    BinDir := IniFile.ReadString('Directories','BinDir',BinDir);
    PasDir := IniFile.ReadString('Directories','PasDir',PasDir);
    LibDir := IniFile.ReadString('Directories','LibDir',LibDir);
  end;

end;

procedure TCompForm.FormActivate(Sender: TObject);
var IniFile : TIniFile;
begin
  IniFileName := MainForm.DevPasPath + ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);

   { Read from the ini file the user's desired compiler options }

  DelphiExt.Checked := IniFile.ReadBool('Pascal_Compiler','DelphiExt',True);
  Assert.Checked := IniFile.ReadBool('Pascal_Compiler','Assert',False);
  Ansi.Checked := IniFile.ReadBool('Pascal_Compiler','Ansi',False);
  TP7.Checked := IniFile.ReadBool('Pascal_Compiler','TP7',True);
  COp.Checked := IniFile.ReadBool('Pascal_Compiler','COp',False);
  LabelGoto.Checked := IniFile.ReadBool('Pascal_Compiler','LabelGoto',True);
  DelphiComp.Checked := IniFile.ReadBool('Pascal_Compiler','DelphiComp',True);
  CInline.Checked := IniFile.ReadBool('Pascal_Compiler','CInline',False);

  If ThisCompiler = GPC
     then Macros.Checked := IniFile.ReadBool('Pascal_Compiler','Macros',True)
     else Macros.Checked := IniFile.ReadBool('Pascal_Compiler','Macros',False);

  LinkDyn.Checked := IniFile.ReadBool('Pascal_Compiler','LinkDyn',False);
  LinkSmart.Checked := IniFile.ReadBool('Pascal_Compiler','LinkSmart',False);
  StripSymbols.Checked := IniFile.ReadBool('Pascal_Compiler','StripSymbols',False);
  NoCheckUnit.Checked := IniFile.ReadBool('Pascal_Compiler','NoCheckUnit',True);

  IOCheck.Checked := IniFile.ReadBool('Code_Generation','IOCheck',False);
  StackCheck.Checked := IniFile.ReadBool('Code_Generation','StackCheck',False);
  Overflow.Checked := IniFile.ReadBool('Code_Generation','Overflow',False);

  GUIApp.Checked := IniFile.ReadBool('Code_Generation','GUI',False);
  CPURadioGroup.ItemIndex := IniFile.ReadInteger('Code_Generation','Target_Cpu', 0);
  
  SmallCode.Checked := IniFile.ReadBool('Optimization','SmallCode',False);
  QuickOpt.Checked := IniFile.ReadBool('Optimization','QuickOpt',True);
  Best.Checked := IniFile.ReadBool('Optimization','Best',False);

  DebugInfo.Checked := IniFile.ReadBool('Linker','DebugInfo',False);
  NoReloc.Checked := IniFile.ReadBool('Linker','NoReloc',False);

  AddDir.Checked := IniFile.ReadBool('Directories','AddDir',AddDir.Checked);
  AddDirEdit.Text := IniFile.ReadString('Directories','Dir',AddDirEdit.Text);
  ComCheck.Checked := IniFile.ReadBool('Directories','AddCommands',ComCheck.Checked);
  Commands.Text := IniFile.ReadString('Directories','Commands',Commands.Text);

  eBinDir.Text := IniFile.ReadString('Directories','BinDir',eBinDir.Text);
  ePasDir.Text := IniFile.ReadString('Directories','PasDir',ePasDir.Text);
  eLibDir.Text := IniFile.ReadString('Directories','LibDir',eLibDir.Text);

  DoInit (ThisCompiler, False);
  ThisCompilerRadioGroup.ItemIndex := Ord (ThisCompiler);
end;

procedure TCompForm.BitBtn3Click(Sender: TObject);
begin
  { Set default compiler options }
   DoInit (ThisCompiler, True);
   eBinDir.Text := AddBackSlash (ExtractFilePath(Application.ExeName)+'Bin');
   ePasDir.Text := AddBackSlash (ExtractFilePath(Application.ExeName)+'Units');
   eLibDir.Text := AddBackSlash (ExtractFilePath(Application.ExeName)+'Units');
end;

procedure TCompForm.HelpBtnClick(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\DevPas.hlp';
  Application.HelpJump('CompilerOptions');
end;

procedure TCompForm.ThisCompilerRadioGroupClick(Sender: TObject);
begin

   ThisCompiler := Compiler_Support (ThisCompilerRadioGroup.ItemIndex);

   DoInit (ThisCompiler, False);

   With MainForm
   do begin
     Case ThisCompiler of
       GPC :
       begin
           CompilerString := ' (GNU Pascal)';
           Macros.Checked := True;
           MingwcompilerhomepageItem.Caption := '&Mingw homepage';
           GNUPascalHelpitem.Visible := True;
       end; // GPC

      FreePascal :
      begin
           CompilerString := ' (Freepascal)';
           MingwcompilerhomepageItem.Caption := 'Freepascal homepage';
           GNUPascalHelpitem.Visible := False;
      end; // Freepascal
     end; // case
     Caption := 'Dev-Pascal '+ DevPasVersion + CompilerString;
   end; //with Mainform
end;

end.

