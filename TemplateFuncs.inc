procedure AddTemplate(AFileName: String);
var
  Template: TTemplate;
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(AFileName);
  Template := TTemplate.Create;
  with Template do
  with Ini do
  begin
     Template.FileName := AFileName;
     Name := ReadString('Template', 'Name', 'Template');
     Icon := ExtractFilePath(ParamStr(0)) + 'Templates\' + ReadString('Template', 'Icon', '');
     Description := ReadString('Template', 'Description', 'No description available.');
     Catagory := ReadString('Template', 'Catagory', 'Additional');
     Text := ReadString('Editor', 'Text', ChangeFileExt(ExtractFileName(AFileName), '.txt'));
     Text := ExtractFilePath(ParamStr(0)) + 'Templates\' + Text;
     CursorX := ReadInteger('Editor', 'CursorX', 1);
     CursorY := ReadInteger('Editor', 'CursorY', 1);
     Console := ReadBool('Project', 'Console', True);
     Dll := ReadBool('Project', 'Dll', False);
     CompilerOptions := ReadString('Project', 'CompilerOptions', '');
     IncludeDirs := ReadString('Project', 'IncludeDirs', '');
     Libs := ReadString('Project', 'Libs', '');
     ProjectName := ReadString('Project', 'Name', '');
  end;
  Templates.Add(Template);
  Ini.Free;
end;

procedure SortCatagories;
var
  Catagories: TStringList;
  Template: TTemplate;
  i: Integer;
  Tab: TTabSheet;
  ListView: TListView;
begin
  Catagories := TStringList.Create;

  for i := 0 to Templates.Count - 1 do
  begin
     Template := Templates.Items[i];
     if Catagories.IndexOf(Template.Catagory) = -1 then
        Catagories.Add(Template.Catagory);
  end;
  for i := 0 to Catagories.Count - 1 do
  begin
     Tab := TTabSheet.Create(Self);
     Tab.Caption := Catagories.Strings[i];
     Tab.PageControl := PageControl;
     ListView := TListView.Create(Self);
     ListView.Align := alClient;
     ListView.ViewStyle := vsIcon;
     ListView.LargeImages := ImageList1;
     ListView.ReadOnly := True;
     ListView.OnChange := ListViewChange;
     ListView.OnDblClick := ListViewDblClick;
     ListView.OnKeyDown := CustomTKeyDown;
     Tab.InsertControl(ListView);
  end;

  Catagories.Free;
end;

procedure SortTemplates;
var
  i, j: Integer;
  ListView: TListView;
  Item: TListItem;
  Template: TTemplate;
  Picture: TPicture;
  Bitmap: TBitmap;
begin
  for i := 0 to Templates.Count - 1 do
  begin
     Template := Templates.Items[i];
     for j := 1 to PageControl.PageCount - 1 do
        if PageControl.Pages[j].Caption = Template.Catagory then
        begin
           ListView := TListView(PageControl.Pages[j].Controls[0]);
           Item := ListView.Items.Add;
           Item.Caption := Template.Name;
           Item.ImageIndex := 0;
           Item.Data := Template;

           if FileExists (Template.Icon) then
           begin
             Picture := TPicture.Create;
             Picture.LoadFromFile(Template.Icon);
             Bitmap := TBitmap.Create;
             Bitmap.Height := Picture.Height;
             Bitmap.Width := Picture.Width;
             Bitmap.Canvas.Draw (0, 0, Picture.Graphic);
             ImageList1.Add(Bitmap, nil);
             Bitmap.Free;
             Picture.Free;
             Item.ImageIndex := ImageList1.Count - 1;
           end;
        end;
  end;
end;
