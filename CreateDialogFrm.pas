unit CreateDialogFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Spin, StdCtrls, Buttons;

type
  TCreateDialogForm = class(TForm)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    ID: TEdit;
    Label1: TLabel;
    Title: TEdit;
    Label2: TLabel;
    FontName: TEdit;
    Label3: TLabel;
    FontSize: TSpinEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    LeftValue: TSpinEdit;
    Label6: TLabel;
    TopValue: TSpinEdit;
    Label7: TLabel;
    HeightValue: TSpinEdit;
    Label8: TLabel;
    WidthValue: TSpinEdit;
    GroupBox3: TGroupBox;
    SysMenu: TCheckBox;
    Min: TCheckBox;
    Maxi: TCheckBox;
    VertScroll: TCheckBox;
    HorizScroll: TCheckBox;
    GroupBox4: TGroupBox;
    TitleBar: TCheckBox;
    Modal: TCheckBox;
    Resizable: TCheckBox;
    ClientEdge: TCheckBox;
    StaticEdge: TCheckBox;
    Popup: TCheckBox;
    CancelBtn: TBitBtn;
    OKBtn: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreateDialogForm: TCreateDialogForm;

implementation

{$R *.DFM}

end.
