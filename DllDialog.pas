unit DllDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, marsCap;

type
  TDllForm = class(TForm)
    OpenDialog: TOpenDialog;
    Image1: TImage;
    Panel1: TPanel;
    Label2: TLabel;
    DllGroup: TGroupBox;
    Label1: TLabel;
    DllName: TEdit;
    Label3: TLabel;
    DllFileName: TEdit;
    Panel2: TPanel;
    BrowseDllBtn: TSpeedButton;
    Label4: TLabel;
    DllSourceName: TEdit;
    DllTestGroup: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    TestDllName: TEdit;
    TestFileName: TEdit;
    Panel3: TPanel;
    BrowseTestBtn: TSpeedButton;
    TestSourceName: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BrowseDllBtnClick(Sender: TObject);
    procedure BrowseTestBtnClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  DllForm: TDllForm;

implementation

{$R *.DFM}

procedure TDllForm.BrowseDllBtnClick(Sender: TObject);
begin
  OpenDialog.Title := 'Create new DLL project';
  if OpenDialog.Execute then
  begin
     DllFileName.Text := OpenDialog.FileName;
     if TestFileName.Text = '' then
        TestFileName.Text := ExtractFilePath(DllFileName.Text);
  end;
end;

procedure TDllForm.BrowseTestBtnClick(Sender: TObject);
begin
  OpenDialog.Title := 'Create new test DLL project';
  if OpenDialog.Execute then
  begin
     TestFileName.Text := OpenDialog.FileName;
     if DllFileName.Text = '' then
     DllFileName.Text := ExtractFilePath(TestFileName.Text);
  end;
end;

end.
