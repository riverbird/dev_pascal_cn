�
 TMESSAGEBOXFORM 0�  TPF0TMessageBoxFormMessageBoxFormLeft� TopuBorderIconsbiSystemMenu BorderStylebsSingleCaption   �[݋Fh��hVClientHeightrClientWidthBColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	Icon.Data
�           �     (       @         �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ���                                                                                                                 x������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������wwwwwwwwwwwwwwwxx�xxx���wwwwwwwxwwwwwwwwwwwwwwwx��������������������������������                                                                ����������������������������                                                                                    ����������������OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthLHeightCaption
   �[݋Fhh��( & T ) : FocusControlTitleFont.CharsetDEFAULT_CHARSET
Font.Colorf f Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTop8Width?HeightCaption   �[݋Fh�Q�[: FocusControlTextMemoFont.CharsetDEFAULT_CHARSET
Font.Colorf f Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TBevelBevel1LeftTopFWidth1HeightShapebsBottomLine  TEditTitleLeftTopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.Color UFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TMemoTextMemoLeftTopHWidth� Height� 
ScrollBarsssBothTabOrder  	TGroupBox	GroupBox1Left TopWidth� HeightqCaption   �[݋Fh	c��TabOrder TLabelLabel4LeftTopWidth(HeightCaption   按钮(&B):FocusControlDialogButtonsFont.CharsetDEFAULT_CHARSET
Font.Colorf f Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTop@Width3HeightCaption   ؞��	c��: FocusControl	DefButtonFont.CharsetDEFAULT_CHARSET
Font.Colorf f Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxDialogButtonsLeftTop Width� HeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder Items.StringsOK
OK, CancelYes, NoYes, No, CancelRetry, CancelAbort, Retry, Ignore   	TComboBox	DefButtonLeftTopPWidth� HeightStylecsDropDownList
ItemHeightTabOrderItems.StringsButton 1Button 2Button 3Button 4    	TGroupBox	GroupBox2Left Top� Width� Height� Caption   �[݋Fh�VhTabOrder TPanelPanel1Left,TopWidthIHeight9
BevelOuter	bvLoweredTabOrder  TImageImage1LeftTopWidthGHeight7AlignalClientCenter	   	TComboBox
DialogIconLeftTop`Width� HeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrderOnChangeDialogIconChangeItems.StringsNoneWarningInformationQuestionStop    TRadioGroup
DialogTypeLeft TopWidth� Height1Caption   !j_{|�W	ItemIndex Items.Strings   (&A)应用程序模式   (&S)系统模式 TabOrder  TBitBtnOKLeftTopPWidthaHeightCaption
   确定(&O)ModalResultTabOrder
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnCancelLeftpTopPWidthaHeightCaption
   取消(&C)TabOrderKindbkCancel  TBitBtnPreviewLeft�TopPWidthaHeightCaption
   预览(&P)TabOrder	OnClickPreviewClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333�333333<333333�w�3333<���33337w73333�3333ssw�33< ����33�w3773<����37w?33sw�������ss�377 �����w37?33sw������s�3s�3770������7?37?3?s3��� 33s�3s�w330��� 3337?37w3333�33333s�s333330 3333337w333333333333333333333333333333333333	NumGlyphs  	TGroupBoxAdvGroupLeft�Top Width� HeightCaptionAdvancedEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclGrayFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder TLabelLabel3LeftTopWidthrHeightCaption* = Windows 95/98 onlyFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TCheckBox
HelpButtonLeftTophWidth� HeightHintOTurn on this option will add a button with
the text "Help" to the message box.CaptionAdd &Help Button*TabOrder  	TCheckBoxRJTextLeftTopzWidth� HeightHint/All text in the message box is right-justified.Caption&Right-Justified Text*TabOrder  	TCheckBoxRTLReadLeftTop� Width� HeightHintbDisplays message and caption text using right-to-left
reading order on Hebrew and Arabic systems.CaptionRight-&To-Left Reading*TabOrder  	TCheckBox	ForgroundLeftTop� Width� HeightHint.The message box becomes the foreground window.CaptionDisplay As &ForegroundTabOrder  TPanelPanel2LeftTopWidth~HeightS
BevelOuter	bvLoweredColor��� TabOrder  TLabelLabel6LeftTopWidth{HeightOAutoSizeCaption|If you want more information about an option, just move your mouse over that checkbox and you will get information about it!WordWrap	   	TCheckBox	TaskModalLeftTop� Width� HeightHintk  Same as an System Modal message box except that all
the top-level windows belonging to the current task are
disabled if the hWnd parameter is NULL. Use this flag
when the calling application or library does not have a
window handle available but still needs to prevent input
to other windows in the current application without
suspending other applications.Caption&Task ModalTabOrder   	TCheckBox	CheckBox1Left�TopWidth� HeightCaption
   O(uؚ�~	�y�( & O ) Font.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickCheckBox1Click   