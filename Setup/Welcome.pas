unit Welcome;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TWelcomeWindow = class(TForm)
    Bevel1: TBevel;
    CloseBtn: TButton;
    ContinueBtn: TButton;
    Label1: TLabel;
    ComboBox1: TComboBox;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ContinueBtnClick(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WelcomeWindow: TWelcomeWindow;

implementation

uses Main, Dest, EULA;

{$R *.DFM}

procedure TWelcomeWindow.FormCreate(Sender: TObject);
begin
 Label1.Width:=WelcomeWindow.ClientWidth-Label1.Left;
end;

procedure TWelcomeWindow.ContinueBtnClick(Sender: TObject);
begin
 MainWindow.Go2;
 Close;
end;

procedure TWelcomeWindow.CloseBtnClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TWelcomeWindow.ComboBox1Change(Sender: TObject);
begin
  Lang:=WelcomeWindow.ComboBox1.Text+'Dialogs';
  WW1:=MainWindow.IniFile.ReadString(Lang,'Welcome1','');
  WW2:=MainWindow.IniFile.ReadString(Lang,'Welcome2','');
  WW3:=MainWindow.IniFile.ReadString(Lang,'Welcome3','');
  WW4:=MainWindow.IniFile.ReadString(Lang,'Welcome4','');
  DW1:=MainWindow.IniFile.ReadString(Lang,'Destination1','');
  DW2:=MainWindow.IniFile.ReadString(Lang,'Destination2','');
  DW3:=MainWindow.IniFile.ReadString(Lang,'Destination3','');
  DW4:=MainWindow.IniFile.ReadString(Lang,'Destination4','');
  DW5:=MainWindow.IniFile.ReadString(Lang,'Destination5','');
  EW1:=MainWindow.IniFile.ReadString(Lang,'EULA1','');
  EW2:=MainWindow.IniFile.ReadString(Lang,'EULA2','');
  EW3:=MainWindow.IniFile.ReadString(Lang,'EULA3','');
  EW4:=MainWindow.IniFile.ReadString(Lang,'EULA4','');
  EW5:=MainWindow.IniFile.ReadString(Lang,'EULA5','');

  WelcomeWindow.Caption:=WW1;
  WelcomeWindow.Label1.Caption:=WW2;
  WelcomeWindow.CloseBtn.Caption:=WW3;
  WelcomeWindow.ContinueBtn.Caption:=WW4;
  DestWindow.Caption:=DW1;
  DestWindow.Label1.Caption:=DW2;
  DestWindow.Label2.Caption:=DW3;
  DestWindow.Button1.Caption:=DW4;
  DestWindow.Button2.Caption:=DW5;
  EULAWindow.Caption:=EW1;
  EULAWindow.Label1.Caption:=EW2;
  EULAWindow.RadioButton1.Caption:=EW3;
  EULAWindow.RadioButton2.Caption:=EW4;
  EULAWindow.Button1.Caption:=EW5;
  Label1.Width:=WelcomeWindow.ClientWidth-Label1.Left;
end;

end.
