object ProgressForm: TProgressForm
  Left = 263
  Top = 184
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Copying files...'
  ClientHeight = 74
  ClientWidth = 271
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 271
    Height = 74
    Align = alClient
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 0
    object Text: TLabel
      Left = 8
      Top = 16
      Width = 102
      Height = 13
      Caption = 'Creating directories ...'
    end
    object ProgressBar: TProgressBar
      Left = 8
      Top = 40
      Width = 255
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 0
    end
  end
end
