object EULAWindow: TEULAWindow
  Left = 202
  Top = 137
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'EULAWindow'
  ClientHeight = 324
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 0
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Memo1: TMemo
    Left = 6
    Top = 18
    Width = 515
    Height = 229
    TabOrder = 0
    WantTabs = True
  end
  object Button1: TButton
    Left = 6
    Top = 294
    Width = 75
    Height = 25
    Caption = 'Button1'
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object RadioButton1: TRadioButton
    Left = 6
    Top = 254
    Width = 509
    Height = 17
    Caption = 'RadioButton1'
    TabOrder = 2
  end
  object RadioButton2: TRadioButton
    Left = 6
    Top = 272
    Width = 511
    Height = 17
    Caption = 'RadioButton2'
    TabOrder = 3
  end
end
