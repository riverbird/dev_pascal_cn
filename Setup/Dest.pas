unit Dest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TDestWindow = class(TForm)
    Bevel1: TBevel;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DestWindow: TDestWindow;

implementation

uses Main, Browse;

{$R *.DFM}

procedure TDestWindow.FormCreate(Sender: TObject);
begin
 Label1.Width:=DestWindow.ClientWidth-2;
end;

procedure TDestWindow.Button1Click(Sender: TObject);
begin
 MainWindow.Go3;
 MainWindow.InstDir := Edit1.Text;
 Close;
end;

procedure TDestWindow.Button2Click(Sender: TObject);
begin
 BrowseWindow.Show;
end;

end.
