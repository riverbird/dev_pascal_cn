unit Browse;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, FileCtrl;

type
  TBrowseWindow = class(TForm)
    Drive: TDriveComboBox;
    DirBox: TDirectoryListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  BrowseWindow: TBrowseWindow;

implementation

uses Dest;

{$R *.DFM}

procedure TBrowseWindow.BitBtn1Click(Sender: TObject);
begin
 DestWindow.Edit1.Text :=DirBox.Directory;
 Close;
end;

procedure TBrowseWindow.BitBtn2Click(Sender: TObject);
begin
 Close;
end;

end.
