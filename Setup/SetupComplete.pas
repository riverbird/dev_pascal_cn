unit SetupComplete;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TSetupCompletedForm = class(TForm)
    Bevel1: TBevel;
    Image1: TImage;
    Label1: TLabel;
    AppLabel: TLabel;
    Label2: TLabel;
    ExitBtn: TBitBtn;
    ExeCheck: TCheckBox;
    RebootCheck: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SetupCompletedForm: TSetupCompletedForm;

implementation

{$R *.DFM}

end.
