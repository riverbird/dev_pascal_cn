unit EULA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TEULAWindow = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EULAWindow: TEULAWindow;

implementation

uses Main;

{$R *.DFM}

procedure TEULAWindow.Button1Click(Sender: TObject);
begin
 if RadioButton1.Checked then
 begin
  Close;
  MainWindow.Go4;
 end;
 if RadioButton2.Checked then
 begin
  Halt(0);
 end;
end;

end.
