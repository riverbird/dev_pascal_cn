object DestWindow: TDestWindow
  Left = 134
  Top = 138
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'DestWindow'
  ClientHeight = 310
  ClientWidth = 512
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 6
    Top = 6
    Width = 501
    Height = 263
  end
  object Label1: TLabel
    Left = 10
    Top = 10
    Width = 39
    Height = 13
    Caption = 'Label1'
    Constraints.MaxWidth = 490
    WordWrap = True
  end
  object Label2: TLabel
    Left = 10
    Top = 126
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object Button1: TButton
    Left = 6
    Top = 278
    Width = 75
    Height = 25
    Caption = 'Button1'
    Default = True
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 10
    Top = 142
    Width = 399
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object Button2: TButton
    Left = 422
    Top = 142
    Width = 75
    Height = 21
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
end
