program Setup;

uses
  Forms,
  Main in 'Main.pas' {MainWindow},
  Welcome in 'Welcome.pas' {WelcomeWindow},
  Dest in 'Dest.pas' {DestWindow},
  EULA in 'EULA.pas' {EULAWindow},
  Browse in 'Browse.pas' {BrowseWindow},
  Progress in 'Progress.pas' {ProgressForm},
  SetupComplete in 'SetupComplete.pas' {SetupCompletedForm};

{$E exe}

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Setup';
  Application.CreateForm(TMainWindow, MainWindow);
  Application.CreateForm(TWelcomeWindow, WelcomeWindow);
  Application.CreateForm(TDestWindow, DestWindow);
  Application.CreateForm(TEULAWindow, EULAWindow);
  Application.CreateForm(TBrowseWindow, BrowseWindow);
  Application.CreateForm(TProgressForm, ProgressForm);
  Application.CreateForm(TSetupCompletedForm, SetupCompletedForm);
  Application.Run;
end.
