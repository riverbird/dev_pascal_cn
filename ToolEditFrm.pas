unit ToolEditFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TToolEditForm = class(TForm)
    Label1: TLabel;
    Title: TEdit;
    Label2: TLabel;
    ProgramEdit: TEdit;
    OpenDialog: TOpenDialog;
    Label3: TLabel;
    WorkDir: TEdit;
    Label4: TLabel;
    Params: TEdit;
    CancelBtn: TBitBtn;
    OkBtn: TBitBtn;
    BrowseBtn: TBitBtn;
    Panel1: TPanel;
    Label5: TLabel;
    MacroList: TListBox;
    InsertBtn: TBitBtn;
    HelpBtn: TBitBtn;
    procedure BrowseBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure HelpClick(Sender: TObject);
    procedure InsertBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ToolEditForm: TToolEditForm;

implementation

{$R *.DFM}

procedure TToolEditForm.BrowseBtnClick(Sender: TObject);
begin
  if OpenDialog.Execute then begin
     ProgramEdit.Text := OpenDialog.FileName;
     WorkDir.Text := ExtractFilePath(OpenDialog.FileName);
  end;
end;

procedure TToolEditForm.CancelBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TToolEditForm.HelpClick(Sender: TObject);
begin
  Application.MessageBox(
              'You can use macros when calling a tool, that it can acts depending on what your doing'+#10#13+
              'in Dev-Pascal. For example, if you are willing to add a tool to Dev-Pascal that can compress'+#10#13+
              'executable files, you may need to know the filename of your project''s executable that'+#10#13+
              'when calling the tool it automatically compress the current project''s executable.'+#10#13+
              'You can use many different parameters macros for your tool, for more information on'+#10#13+
              'what they can do see the Macro lists on the previous dialog.'
  ,'Quick help on macros', MB_ICONINFORMATION);
end;

procedure TToolEditForm.InsertBtnClick(Sender: TObject);
begin
  case MacroList.ItemIndex of
  0 : Params.Text := Params.Text+'<EXENAME>';
  1 : Params.Text := Params.Text+'<PROJECTNAME>';
  2 : Params.Text := Params.Text+'<PROJECTPATH>';
  3 : Params.Text := Params.Text+'<CURRENTSOURCENAME>';
  end;
end;

end.
