unit SetupCreatorAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, marsCap, ExtCtrls;

type
  TAboutSetupCreatorWindow = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    MarsCaption1: TMarsCaption;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutSetupCreatorWindow: TAboutSetupCreatorWindow;

implementation

{$R *.DFM}

procedure TAboutSetupCreatorWindow.Button1Click(Sender: TObject);
begin
 Close;
end;

end.
