unit ProjectOptions;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtDlgs, ExtCtrls, StdCtrls, Buttons, marsCap;

type
  TProjectOpt = class(TForm)
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    Panel: TPanel;
    GroupBox1: TGroupBox;
    LoadBtn: TBitBtn;
    Panel1: TPanel;
    Icon: TImage;
    OpenPicture: TOpenPictureDialog;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    ObjFileName: TEdit;
    Label1: TLabel;
    OpenDialog: TOpenDialog;
    BitBtn1: TBitBtn;
    Label2: TLabel;
    ProjectName: TEdit;
    NoConsole: TCheckBox;
    CreateDll: TCheckBox;
    IconLibBtn: TBitBtn;
    ResFiles: TEdit;
    BitBtn2: TBitBtn;
    Label3: TLabel;
    HelpBtn: TBitBtn;
    Label4: TLabel;
    CompilerOpt: TEdit;
    Label5: TLabel;
    IncDir: TEdit;
    procedure LoadBtnClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure IconLibBtnClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    IconFileName : string;
    { Déclarations publiques }
  end;

var
  ProjectOpt: TProjectOpt;

implementation

uses IconFrm;

{$R *.DFM}

procedure TProjectOpt.LoadBtnClick(Sender: TObject);
begin
if OpenPicture.Execute then
  begin
    Icon.Picture.LoadFromFile(OpenPicture.FileName);
    IconFileName := OpenPicture.FileName;
  end;
end;

procedure TProjectOpt.BitBtn1Click(Sender: TObject);
var
  i: Integer;
  Txt: String;
begin
  { Modified by Hongli }
  { The user can now select more than one file in
    the OpenDialog }

  OpenDialog.FilterIndex := 1;
  Txt := '';
  if OpenDialog.Execute then begin
     for i := 0 to OpenDialog.Files.Count - 1 do
        Txt := Txt + ' ' + OpenDialog.Files.Strings[i];
     Delete(Txt, 1, 1);
     ObjFileName.Text := ObjFileName.Text+' '+Txt;
  end;
end;

procedure TProjectOpt.IconLibBtnClick(Sender: TObject);
begin
  if IconForm.ShowModal = MrOK then begin
     IconFileName := IconLib[SelectedIndex-1]+'.ico';
     Icon.Picture.LoadFromFile(IconFileName);
  end;
end;

procedure TProjectOpt.BitBtn2Click(Sender: TObject);
var
  i: Integer;
  Txt: String;
begin
  Txt := '';
  OpenDialog.FilterIndex := 4;
  if OpenDialog.Execute then begin
     for i := 0 to OpenDialog.Files.Count - 1 do
        Txt := Txt + ' ' + OpenDialog.Files.Strings[i];
     Delete(Txt, 1, 1);
     ResFiles.Text := ResFiles.Text+' '+Txt;
  end;
end;

procedure TProjectOpt.HelpBtnClick(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\Tutorial.hlp';
  Application.HelpJump('IDH_810');
end;

end.
