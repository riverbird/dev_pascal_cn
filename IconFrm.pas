unit IconFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, Main, ImgList, Version;

type
  TIconForm = class(TForm)
    IconView: TListView;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    ImageList: TImageList;
    procedure OkBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

const
  NumIcons = 16;

var
  IconLib : array [0..NumIcons] of string;
  SelectedIndex : integer;

var
  IconForm: TIconForm;

implementation

{$R *.DFM}

procedure TIconForm.OkBtnClick(Sender: TObject);
begin
  if IconView.Selected = nil then
  begin
    ModalResult := mrNone;
    exit;
  end;

  SelectedIndex := IconView.ItemFocused.Index + 1;
end;

procedure TIconForm.FormActivate(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to NumIcons-1 do
     IconLib[i] := ExtractFilePath(Application.ExeName) + ICON_DIR + IconView.Items.Item[i].Caption;
end;

end.
