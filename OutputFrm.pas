unit OutputFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Menus, marsCap;

type
  TOutputForm = class(TForm)
    Output: TRichEdit;
    PopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    N1: TMenuItem;
    Selectall1: TMenuItem;
    Saveastextfile1: TMenuItem;
    SaveDialog: TSaveDialog;
    procedure Copy1Click(Sender: TObject);
    procedure Selectall1Click(Sender: TObject);
    procedure Saveastextfile1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  OutputForm: TOutputForm;

implementation

{$R *.DFM}

procedure TOutputForm.Copy1Click(Sender: TObject);
begin
  Output.CopyToClipboard;
end;

procedure TOutputForm.Selectall1Click(Sender: TObject);
begin
  Output.SelectAll;
end;

procedure TOutputForm.Saveastextfile1Click(Sender: TObject);
begin
  if SaveDialog.Execute then
     Output.Lines.SaveToFile(SaveDialog.FileName);
end;

end.
