unit SetMainUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TMainUnitForm = class(TForm)
    GroupBox: TGroupBox;
    UnitList: TListBox;
    Panel: TPanel;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainUnitForm: TMainUnitForm;

implementation

{$R *.DFM}

end.
