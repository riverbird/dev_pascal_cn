unit FirstFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Menus, ExtCtrls;

type
  TFirstForm = class(TForm)
    Label1: TLabel;
    GroupBox1: TGroupBox;
    AssignDev: TCheckBox;
    AssignC: TCheckBox;
    IconGroupBox: TGroupBox;
    IconBox: TComboBox;
    OkBtn: TBitBtn;
    Label2: TLabel;
    PopupMenu: TPopupMenu;
    PreviewBtn: TBitBtn;
    N1: TMenuItem;
    Panel1: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    procedure FormActivate(Sender: TObject);
    procedure PreviewBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FirstForm: TFirstForm;

implementation

uses Main;

{$R *.DFM}

procedure TFirstForm.FormActivate(Sender: TObject);
begin
  IconBox.ItemIndex := 1;
end;

procedure TFirstForm.PreviewBtnClick(Sender: TObject);
begin
{$ifndef ver100}
  if IconBox.Text = 'Gnome' then
     PopupMenu.Images := MainForm.MenuImages_Gnome
  else PopupMenu.Images := MainForm.MenuImages_Default;
{$endif}
  PopupMenu.Popup(Left+PreviewBtn.Left+IconGroupBox.Left+PreviewBtn.Width+15, Top+PreviewBtn.Top+IconGroupBox.Top);
end;

end.
