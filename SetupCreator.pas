unit SetupCreator;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls, StdCtrls, IniFiles, Backup, Utils, marsCap, Buttons,
  CheckLst, ComCtrls, TB97, ImgList, TB97Ctls, TB97Tlbr;

type
  TSetupCreatorWindow = class(TForm)
    BackBtn: TButton;
    NextBtn: TButton;
    Notebook: TNotebook;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BrowseInfo: TButton;
    Label8: TLabel;
    AppTitle: TEdit;
    Label9: TLabel;
    Version: TEdit;
    Label10: TLabel;
    AppExe: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    Label14: TLabel;
    LangBox: TComboBox;
    Label15: TLabel;
    Title: TEdit;
    Label16: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Label17: TLabel;
    DefaultDir: TEdit;
    Label18: TLabel;
    CompanyName: TEdit;
    Label19: TLabel;
    GroupBox: TListBox;
    Label20: TLabel;
    PopupMenu: TPopupMenu;
    RemoveBoxItem: TMenuItem;
    PopupMenu2: TPopupMenu;
    RemovePopItem: TMenuItem;
    PropertiesBoxItem: TMenuItem;
    N1: TMenuItem;
    OpenDialog: TOpenDialog;
    Label21: TLabel;
    SaveDialog: TSaveDialog;
    Compress: TBackupFile;
    Label22: TLabel;
    Memo: TMemo;
    CloseBtn: TBitBtn;
    Bevel1: TBevel;
    Panel1: TPanel;
    Image1: TImage;
    FilesBox: TListBox;
    AboutBtn: TBitBtn;
    Label1: TLabel;
    BuildBtn: TBitBtn;
    TestBtn: TBitBtn;
    ProgressBar: TProgressBar;
    ProgressLabel: TLabel;
    PopupMenu1: TPopupMenu;
    PopupMenu3: TPopupMenu;
    New: TMenuItem;
    Open1: TMenuItem;
    N2: TMenuItem;
    Close1: TMenuItem;
    About1: TMenuItem;
    Dock971: TDock97;
    Toolbar971: TToolbar97;
    ToolbarButton971: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    OpenFileDialog: TOpenDialog;
    PropertiesBtn: TBitBtn;
    RemoveBtn: TBitBtn;
    NewGroupBtn: TBitBtn;
    InsertFileBtn: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BackBtnClick(Sender: TObject);
    procedure NextBtnClick(Sender: TObject);
    procedure NotebookPageChanged(Sender: TObject);
    procedure BrowseInfoClick(Sender: TObject);
    procedure AppTitleChange(Sender: TObject);
    procedure CompanyNameChange(Sender: TObject);
    procedure InsertFilesBtnClick(Sender: TObject);
    procedure NewGroupBtnClick(Sender: TObject);
    procedure BuildBtnClick(Sender: TObject);
    procedure GroupBoxClick(Sender: TObject);
    procedure RemoveItem(Sender: TObject);
    procedure RemoveBoxItemClick(Sender: TObject);
    procedure PropertiesBoxItemClick(Sender: TObject);
    procedure AboutBtnClick(Sender: TObject);
    procedure FilesBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GroupBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TestBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure CheckBox3Exit(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure NewSetup(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure FilesBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    GroupFocus : boolean;
  end;

var
  SetupCreatorWindow: TSetupCreatorWindow;

implementation

uses Main, SetupCreatorInfo, SetupCreatorAbout, SetupSelectDir;

{$R *.DFM}

procedure TSetupCreatorWindow.FormCreate(Sender: TObject);
begin
  GroupFocus := False;
  if FileExists(GetTempDir+'\setup.tmp') then
     DeleteFile(pchar(GetTempDir+'\setup.tmp'));
  NewSetup(Self);
end;

procedure TSetupCreatorWindow.BackBtnClick(Sender: TObject);
begin
 Notebook.PageIndex:=Notebook.PageIndex-1;
end;

procedure TSetupCreatorWindow.NextBtnClick(Sender: TObject);
begin
 Notebook.PageIndex:=Notebook.PageIndex+1;
end;

procedure TSetupCreatorWindow.NotebookPageChanged(Sender: TObject);
begin
 BackBtn.Enabled := Notebook.PageIndex > 0;
 NextBtn.Enabled := Notebook.PageIndex < Notebook.Pages.Count - 1;

 if Notebook.PageIndex = 3 then
 begin
    if DefaultDir.Text = '' then
    begin
       Application.MessageBox('You must supply a default installation directory.',
         'Human Error!', MB_OK + MB_ICONSTOP);
       Notebook.PageIndex := 2;
    end;
 end;
end;

procedure TSetupCreatorWindow.BrowseInfoClick(Sender: TObject);
begin
  try
    SetupCreatorInfoWindow := TsetupCreatorInfoWindow.Create(self);
    SetupCreatorInfoWindow.ShowModal;
  finally
    SetupCreatorInfoWindow.Free;
  end;
end;

procedure TSetupCreatorWindow.AppTitleChange(Sender: TObject);
begin
 Title.Text :='Setup - '+AppTitle.Text;

 DefaultDir.Text :='c:\Program Files\';
 if CompanyName.Text <> '' then
    DefaultDir.Text :=DefaultDir.Text+CompanyName.Text+'\';

 DefaultDir.Text := DefaultDir.Text+AppTitle.Text;
end;

procedure TSetupCreatorWindow.CompanyNameChange(Sender: TObject);
begin
 DefaultDir.Text :='c:\Program Files\';
 if CompanyName.Text <> '' then
    DefaultDir.Text := DefaultDir.Text+CompanyName.Text+'\';

 DefaultDir.Text := DefaultDir.Text+AppTitle.Text;
end;

procedure TSetupCreatorWindow.InsertFilesBtnClick(Sender: TObject);
var
 IniFile: TIniFile;
 x, i: Integer;
begin
 if OpenDialog.Execute then
 begin
   for i := 0 to OpenDialog.Files.Count-1 do
       FilesBox.Items.Add(OpenDialog.Files[i]);

   IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
   IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'Files',IntToStr(FilesBox.Items.Count));
   for x:=0 to FilesBox.Items.Count-1 do
   begin
     IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'File'+IntToStr(x),FilesBox.Items[x]);
   end;
 end;
end;

procedure TSetupCreatorWindow.NewGroupBtnClick(Sender: TObject);
var
 a: String;
 IniFile: TIniFile;
 x: Integer;
begin
 a:=InputBox('Create Group','Title:','');
 if a <> '' then
 begin
  x:=GroupBox.Items.Count;
  GroupBox.Items.Add(a);
  GroupBox.ItemIndex:=x;
  FilesBox.Items.Clear;
  IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
  IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'Dir','<INSTALLDIR>\');
  IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'Files',IntToStr(FilesBox.Items.Count));
 end;
end;

procedure TSetupCreatorWindow.BuildBtnClick(Sender: TObject);
var
 a,c: String;
 b,IniFile: TIniFile;
 x,y: Integer;
 Destination1,Destination2,Destination3,Destination4,Destination5: String;
 Welcome1,Welcome2,Welcome3,Welcome4: String;
 EULA1,EULA2,EULA3,EULA4,EULA5: String;
 Lang: String;
begin
 ProgressBar.Visible := True;
 ProgressLabel.Visible := True;
 Application.ProcessMessages;

 if FilesBox.Items.Count = 0 then
 begin
  Application.MessageBox('No files to install.','Human Error!',mb_ok+mb_iconstop);
  Notebook.PageIndex:=4;
  exit;
 end;

 if SaveDialog.Execute then
 begin
  a:=ExtractFileDir(SaveDialog.FileName);
  IniFile := TIniFile.Create(a+'\setup.inf');
  IniFile.WriteString('App','Title',AppTitle.Text);
  IniFile.WriteString('App','Version',Version.Text);
  IniFile.WriteString('App','EXEName',AppExe.Text);
  IniFile.WriteString('App','Company',CompanyName.Text);
  ProgressBar.Position:=17;
  IniFile.WriteString('Options','Dir',DefaultDir.Text);
  IniFile.WriteString('Options','Title',Title.Text);
  IniFile.WriteString('Options','Language',LangBox.Text);
  if RadioButton1.Checked then
   IniFile.WriteString('Options','BGColor','Blue');
  if RadioButton2.Checked then
   IniFile.WriteString('Options','BGColor','Green');
  if RadioButton3.Checked then
   IniFile.WriteString('Options','BGColor','Yellow');
  if RadioButton4.Checked then
   IniFile.WriteString('Options','BGColor','Red');
  ProgressBar.Position:=23;
  for x:=0 to LangBox.Items.Count-1 do
  begin
   Lang:=LangBox.Items[x];
 if Lang = 'English' then
 begin
  EULA1:='License Agreement';
  EULA2:='Please read the following license agreement carefully.';
  EULA3:='I &accept the terms in the license agreement';
  EULA4:='I &do not accept the terms in the license agreement';
  EULA5:='&Next >';
  Welcome1:='Welcome to the Setup Program for '+AppTitle.Text+'.';
  Welcome2:='The Setup Program will help install '+AppTitle.Text+' on your computer.   To continue, click Next.';
  Welcome3:='Cancel';
  Welcome4:='&Next >';
  Destination1:='Location to Save Files';
  Destination2:='Please enter the folder where you want these files saved.  If the folder does not exist, it will be created for you.   To continue, click Next.';
  Destination3:='&Save files in folder:';
  Destination4:='Continue';
  Destination5:='&Change...';
 end;

 if Lang = 'German' then
 begin
  EULA1:='Lizenzvereinbarung';
  EULA2:='Bitte lesen Sie die folgende Lizenzvereinbarung sorgf�ltig durch.';
  EULA3:='Ich &akzeptiere die Bedingungen der Lizenzvereinbarung';
  EULA4:='Ich &lehne die Bedingungen der Lizenzvereinbarung ab';
  EULA5:='&Weiter >';
  Welcome1:='Willkommen beim Setup Program f�r '+AppTitle.Text+'.';
  Welcome2:='Mithilfe des Setup Program k�nnen Sie '+AppTitle.Text+' auf Ihrem Computer installieren.   Klicken Sie auf ''Weiter'', um fortzufahren.';
  Welcome3:='Fortfahren';
  Welcome4:='&Weiter >';
  Destination1:='Speicherort f�r Dateien';
  Destination2:='Bitte geben Sie den Ordner ein, in dem Sie Ihre Dateien speichern m�chten. Wenn dieser Ordner noch nicht vorhanden ist, wird er f�r Sie erstellt. Klicken Sie auf ''Weiter'', um fortzufahren.';
  Destination3:='&Dateien in Ordner speichern:';
  Destination4:='Abbrechen';
  Destination5:='&�ndern...';
 end;

 if Lang = 'French' then
 begin
  EULA1:='Contrat de licence';
  EULA2:='Veuillez lire attentivement le contrat de licence suivant.';
  EULA3:='J''&accepte les termes de ce contrat de licence';
  EULA4:='Je &n''accepte pas les termes de ce contrat de licence';
  EULA5:='&Suivant >';
  Welcome1:='Bienvenue dans le programme d''installation pour '+AppTitle.Text+'.';
  Welcome2:='Le programme d''installation vous aidera � installer '+AppTitle.Text+' sur votre ordinateur.   Pour continuer, cliquez sur Suivant.';
  Welcome3:='Annuler';
  Welcome4:='&Suivant >';
  Destination1:='Emplacement pour enregistrer les fichiers';
  Destination2:='Veuillez entrer le dossier dans lequel vous souhaitez enregistrer vos fichiers. Si le dossier n''existe pas, il sera cr�� automatiquement. Pour continuer, cliquez sur Suivant.';
  Destination3:='&Enregistrer les fichiers dans le dossier:';
  Destination4:='Continuer';
  Destination5:='&Modifier...';
 end;

 if Lang = 'Spanish' then
 begin
  EULA1:='Contrato de licencia';
  EULA2:='Lea cuidadosamente el contrato de licencia siguiente.';
  EULA3:='A&cepto los t�rminos del contrato de licencia';
  EULA4:='&No acepto los t�rminos del contrato de licencia';
  EULA5:='&Siguiente >';
  Welcome1:='Bienvenido a Setup Program para '+AppTitle.Text+'.';
  Welcome2:='Setup Program le ayudar� a instalar '+AppTitle.Text+' en su equipo.   Para continuar, haga clic en Siguiente.';
  Welcome3:='Cancelar';
  Welcome4:='&Siguiente >';
  Destination1:='Ubicaci�n para guardar archivos';
  Destination2:='Introduzca la carpeta en la que desee guardar los archivos.  Si la carpeta no existe, se proceder� a su creaci�n.  Para continuar, haga clic en Siguiente.';
  Destination3:='&Guardar archivos en carpeta:';
  Destination4:='&Cambiar...';
  Destination5:='&Change...';
 end;

 if Lang = 'Dutch' then
 begin
  EULA1:='Gebruiksrechtovereenkomst';
  EULA2:='Lees de volgende gebruiksrechtovereenkomst zorgvuldig door.';
  EULA3:='Ik ga &akkoord met de voorwaarden van de gebruiksrechtovereenkomst';
  EULA4:='Ik &ga niet akkoord met de voorwaarden van de gebruiksrechtovereenkomst';
  EULA5:='2=Vo&lgende >';
  Welcome1:='Welkom bij Setup Program voor '+AppTitle.Text+'.';
  Welcome2:='Setup Program helpt u om '+AppTitle.Text+' op uw computer te installeren.   Klik op Volgende om door te gaan.';
  Welcome3:='Annuleren';
  Welcome4:='Vo&lgende >';
  Destination1:='Installatiemap';
  Destination2:='Geef de map op waarin de bestanden wilt opslaan.  Als de map niet bestaat, wordt deze voor u gemaakt.  Klik op Volgende om door te gaan.';
  Destination3:='&Installatiemap:';
  Destination4:='Doorgaan';
  Destination5:='&Wijzigen...';
 end;

 if Lang = 'Indonesian' then
 begin
  EULA1:='Perjanjian Lisensi';
  EULA2:='Mohon baca perjanjian lisensi berikut dengan cermat.';
  EULA3:='Saya&menerima semua hal yang ada dalam perjanjian lisensi';
  EULA4:='Saya&tidak menerima syarat-syarat yang tercantum dalam perjanjian lisensi ini';
  EULA5:='S&elanjutnya >';
  Welcome1:='Selamat datang di Program Setup '+AppTitle.Text+'.';
  Welcome2:='Program Setup akan membantu proses instalasi '+AppTitle.Text+' di komputer anda. Untuk terus, klik Selanjutnya.';
  Welcome3:='Pembatalan';
  Welcome4:='S&elanjutnya >';
  Destination1:='Lokasi untuk menyimpan File';
  Destination2:='Mohon masukkan folder di mana anda ingin file anda disimpan. Jika folder tidak ada, akan dibuat untuk anda. Untuk terus, klik Selanjutnya.';
  Destination3:='S&impan file di folder:';
  Destination4:='Lanjutkan';
  Destination5:='&Mengubah...';
 end;

   IniFile.WriteString(Lang+'Dialogs','EULA1',EULA1);
   IniFile.WriteString(Lang+'Dialogs','EULA2',EULA2);
   IniFile.WriteString(Lang+'Dialogs','EULA3',EULA3);
   IniFile.WriteString(Lang+'Dialogs','EULA4',EULA4);
   IniFile.WriteString(Lang+'Dialogs','EULA5',EULA5);
   IniFile.WriteString(Lang+'Dialogs','Welcome1',Welcome1);
   IniFile.WriteString(Lang+'Dialogs','Welcome2',Welcome2);
   IniFile.WriteString(Lang+'Dialogs','Welcome3',Welcome3);
   IniFile.WriteString(Lang+'Dialogs','Welcome4',Welcome4);
   IniFile.WriteString(Lang+'Dialogs','Destination1',Destination1);
   IniFile.WriteString(Lang+'Dialogs','Destination2',Destination2);
   IniFile.WriteString(Lang+'Dialogs','Destination3',Destination3);
   IniFile.WriteString(Lang+'Dialogs','Destination4',Destination4);
   IniFile.WriteString(Lang+'Dialogs','Destination5',Destination5);

  end;
 ProgressBar.Position:=45;
  if CheckBox1.Checked=True then
   IniFile.WriteString('Dialogs','Destination','True')
  else
   IniFile.WriteString('Dialogs','Destination','False');
  if CheckBox2.Checked=True then
   IniFile.WriteString('Dialogs','Welcome','True')
  else
   IniFile.WriteString('Dialogs','Welcome','False');
  if CheckBox3.Checked=True then
   IniFile.WriteString('Dialogs','EULA','True')
  else
   IniFile.WriteString('Dialogs','EULA','False');

  a:=ExtractFileDir(SaveDialog.FileName);
  ChDir(a);
  if CheckBox3.Checked then
     Memo.Lines.SaveToFile('eula.txt');

  b := TIniFile.Create(GetTempDir + '\setup.tmp');
  IniFile.WriteString('Groups','Count',IntToStr(GroupBox.Items.Count));
  for x:=0 to GroupBox.Items.Count-1 do
  begin
  IniFile.WriteString('Groups','Group'+IntToStr(x),GroupBox.Items[x]);
  if b.ReadString(GroupBox.Items[x],'Dir','') = '' then
   b.WriteString(GroupBox.Items[x],'Dir','<INSTALLDIR>\');
  IniFile.WriteString(GroupBox.Items[x],'Dir',b.ReadString(GroupBox.Items[x],'Dir',''));
  IniFile.WriteString(GroupBox.Items[x],'Files',b.ReadString(GroupBox.Items[x],'Files',''));
  c:='Setup.'+IntToStr(x+1);

   for y:=0 to b.ReadInteger(GroupBox.Items[x],'Files',0)-1 do
    IniFile.WriteString(GroupBox.Items[x],'File'+IntToStr(y),b.ReadString(GroupBox.Items[x],'File'+IntToStr(y),''));

  IniFile.WriteString(GroupBox.Items[x],'Cabinet',c);

  GroupBox.ItemIndex:=x;
  GroupBoxClick(Sender);
  Compress.Backup(FilesBox.Items, c);
  ProgressBar.Position:=73;
  end;
  a:=ExtractFileDir(SaveDialog.FileName);
  ProgressBar.Position:=98;
  CopyFile(MainForm.BinDir+'Setup.exe',a+'\Setup.exe');
  ProgressBar.Position:=100;
  TestBtn.Enabled := True;
  MessageDlg('Setup Created !', mtInformation, [mbOk], 0);
 end;
end;
procedure TSetupCreatorWindow.GroupBoxClick(Sender: TObject);
var
 IniFile: TIniFile;
 x: Integer;
 a: Integer;
begin
 GroupFocus := True;
 IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
 FilesBox.Items.Clear;
 a:=StrToInt(IniFile.ReadString(GroupBox.Items[GroupBox.ItemIndex],'Files',''));
 if a>0 then
 begin
 for x:= 0 to a-1 do
  if IniFile.ReadString(GroupBox.Items[GroupBox.ItemIndex],'File'+IntToStr(x),'') <> '' then
     FilesBox.Items.Add(IniFile.ReadString(GroupBox.Items[GroupBox.ItemIndex],'File'+IntToStr(x),''));
 end;
end;

procedure TSetupCreatorWindow.RemoveItem(Sender: TObject);
var
 IniFile: TIniFile;
 x: Integer;
begin
 FilesBox.Items.Delete(FilesBox.ItemIndex);
 IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
 IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'Files',IntToStr(FilesBox.Items.Count));
 for x:=0 to FilesBOx.Items.Count-1 do
 begin
  IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'File'+IntToStr(x),FilesBox.Items[x]);
 end;
end;

procedure TSetupCreatorWindow.RemoveBoxItemClick(Sender: TObject);
begin
 if GroupFocus then begin
 if GroupBox.ItemIndex < 0 then exit;
 if (MessageDlg('Are you sure you want to remove the '+GroupBox.Items[GroupBox.ItemIndex]+' group ?', mtConfirmation, [mbYes, mbNo], 0) =mrYes)
    and (GroupBox.Items[GroupBox.ItemIndex] <> 'Program Files') then
 begin
  FilesBox.Items.Clear;
  GroupBoxClick(Sender);
  GroupBox.Items.Delete(GroupBox.ItemIndex);
  GroupBox.ItemIndex:=0;
  GroupBoxClick(Sender);
 end;
 end else RemoveItem(sender);
end;

procedure TSetupCreatorWindow.PropertiesBoxItemClick(Sender: TObject);
var
 a,b: String;
 IniFile: TIniFile;
begin
 if GroupBox.ItemIndex < 0 then exit;

 IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
 b:=IniFile.ReadString(GroupBox.Items[GroupBox.ItemIndex],'Dir','');
 if b = '' then
    b:='<INSTALLDIR>\';

 try
   SelectDirForm := TSelectDirForm.Create(self);
   SelectDirForm.InstallDir.Text := b;
   SelectDirForm.ShowModal;

   if SelectDirForm.ModalResult = mrOk then begin
      a := SelectDirForm.InstallDir.Text;
      if a <> '' then
         IniFile.WriteString(GroupBox.Items[GroupBox.ItemIndex],'Dir',a);
   end;
 finally
   SelectDirForm.Free;
 end;

end;

procedure TSetupCreatorWindow.AboutBtnClick(Sender: TObject);
begin
  try
    AboutSetupCreatorWindow := TAboutSetupCreatorWindow.Create(self);
    AboutSetupCreatorWindow.ShowModal;
  finally
    AboutSetupCreatorWindow.Free;
  end;
end;

procedure TSetupCreatorWindow.FilesBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_DELETE) and (FilesBox.SelCount <> 0) then
     RemoveItem(sender);
end;

procedure TSetupCreatorWindow.GroupBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_DELETE) and (GroupBox.SelCount <> 0) then
     RemoveBoxItemClick(sender);
end;

procedure TSetupCreatorWindow.TestBtnClick(Sender: TObject);
begin
  try
    ExecuteFile(ExtractFilePath(SaveDialog.FileName)+'Setup.exe','',ExtractFileDir(SaveDialog.FileName), SW_SHOW);
  except
    MessageDlg('Setup creator was not able to find the setup file.', mtError, [mbOK], 0);
  end;
end;

procedure TSetupCreatorWindow.FormShow(Sender: TObject);
begin
 ProgressBar.Position:=0;
 TestBtn.Enabled := False;
end;

procedure TSetupCreatorWindow.CheckBox3MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 Memo.Enabled:=CheckBox3.Checked;
end;

procedure TSetupCreatorWindow.CheckBox3Exit(Sender: TObject);
begin
 Memo.Enabled:=CheckBox3.Checked;
end;

procedure TSetupCreatorWindow.Close1Click(Sender: TObject);
begin
 Close;
end;

procedure TSetupCreatorWindow.NewSetup(Sender: TObject);
var
  IniFile: TIniFile;
begin
  DeleteFile(GetTempDir + '\Setup.tmp');
  IniFile := TIniFile.Create(GetTempDir + '\setup.tmp');
  IniFile.WriteString('Program Files', 'Files', '0');
  IniFile.WriteString('Program Files', 'Dir', '');
  FilesBox.Items.Clear;
  GroupBox.Items.Clear;
  GroupBox.Items.Add('Program Files');
  GroupBox.ItemIndex := 0;
  Notebook.PageIndex := 0;
  TestBtn.Enabled := False;
  ProgressBar.Visible := False;
  ProgressLabel.Visible := False;
  IniFile.Free;
end;

procedure TSetupCreatorWindow.Open1Click(Sender: TObject);
var
  IniFile: TIniFile;
  k : integer;
begin
if OpenFileDialog.Execute then begin
  DeleteFile(GetTempDir + '\Setup.tmp');
  CopyFile(OpenFileDialog.FileName, GetTempDir + '\Setup.tmp');
  FilesBox.Items.Clear;
  GroupBox.Items.Clear;
  TestBtn.Enabled := False;
  ProgressBar.Visible := False;
  ProgressLabel.Visible := False;

  if FileExists(ExtractFilePath(OpenFileDialog.FileName)+'EULA.txt') then
     Memo.Lines.LoadFromFile(ExtractFilePath(OpenFileDialog.FileName)+'EULA.txt');

  IniFile := TIniFile.Create(OpenFileDialog.FileName);

  AppTitle.Text := IniFile.ReadString('App','Title','');
  Version.Text := IniFile.ReadString('App','Version','1.0');
  CompanyName.Text := IniFile.ReadString('App','Company','');
  AppExe.Text := IniFile.ReadString('App','EXEName','');

  DefaultDir.Text := IniFile.ReadString('Options','Dir','');
  Title.Text := IniFile.ReadString('Options','Title','');
  LangBox.Text := IniFile.ReadString('Options','Language','');

  if IniFile.ReadString('Options','BGColor','') = 'Blue' then
     RadioButton1.Checked := True
  else if IniFile.ReadString('Options','BGColor','') = 'Green' then
     RadioButton2.Checked := True
  else if IniFile.ReadString('Options','BGColor','') = 'Yellow' then
     RadioButton3.Checked := True
  else if IniFile.ReadString('Options','BGColor','') = 'Red' then
     RadioButton4.Checked := True;

  CheckBox1.Checked := IniFile.ReadBool('Dialogs','Destination',True);
  CheckBox2.Checked := IniFile.ReadBool('Dialogs','Welcome',True);
  CheckBox3.Checked := IniFile.ReadBool('Dialogs','EULA',True);

  for k := 0 to IniFile.ReadInteger('Groups','Count',1)-1 do begin
      GroupBox.Items.Add(IniFile.ReadString('Groups','Group'+IntToStr(k),'Program files'));
  end;
  IniFile.Free;
  end;
end;

procedure TSetupCreatorWindow.FormDeactivate(Sender: TObject);
begin
  WindowState := wsMinimized;
end;

procedure TSetupCreatorWindow.NewClick(Sender: TObject);
begin
if MessageDlg('Are you sure you want to create a new setup ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
   NewSetup(sender);
end;

procedure TSetupCreatorWindow.FilesBoxClick(Sender: TObject);
begin
  GroupFocus := False;
end;

end.
