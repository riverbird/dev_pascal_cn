unit Line;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, marsCap, Spin;

type
  TLineForm = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Line: TSpinEdit;
    procedure LineKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LineForm: TLineForm;

implementation

{$R *.DFM}

procedure TLineForm.LineKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = vk_Return then begin
   Close;
   ModalResult := mrOK;
end;
end;

end.
