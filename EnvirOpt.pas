unit EnvirOpt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, IniFiles, Mask,  ComCtrls, Registry, marsCap,
  ColorGrd, Spin, SynEdit, SynCompletionProposal, SynEditHighlighter,
  SynHighlighterPas;

type
  TEnvirForm = class(TForm)
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    DefaultBtn: TBitBtn;
    PageControl: TPageControl;
    Preferences: TTabSheet;
    GroupBox2: TGroupBox;
    SavePos: TCheckBox;
    ColorSheet: TTabSheet;
    DefaultDirectory: TGroupBox;
    Defaultdir: TEdit;
    ExecutionPanel: TGroupBox;
    Minimize: TCheckBox;
    Windows: TGroupBox;
    AutoArrange: TCheckBox;
    TabSheet1: TTabSheet;
    DevFile: TCheckBox;
    ElementList: TListBox;
    ColorGrid: TColorGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Backup: TCheckBox;
    TabSheet2: TTabSheet;
    GroupBox5: TGroupBox;
    GroupBox1: TGroupBox;
    SizeLabel: TLabel;
    FontLabel: TLabel;
    Font: TComboBox;
    Size: TEdit;
    GroupBox3: TGroupBox;
    EditorColor: TComboBox;
    BackupPanel: TGroupBox;
    LineNumbers: TCheckBox;
    GroupBox7: TGroupBox;
    ShowScrollHint: TCheckBox;
    DontShowScroll: TCheckBox;
    NoProjExpl: TCheckBox;
    ExecParam: TCheckBox;
    Params: TEdit;
    BitBtn4: TBitBtn;
    AssignC: TCheckBox;
    TabIndent: TSpinEdit;
    AutoIndent: TCheckBox;
    Label1: TLabel;
    CodeSheet: TTabSheet;
    GroupBox6: TGroupBox;
    Label2: TLabel;
    GroupBox9: TGroupBox;
    Memo: TMemo;
    TestBtn: TBitBtn;
    ToolBarSave: TCheckBox;
    Label3: TLabel;
    SaveEditor: TCheckBox;
    CompileStayOnTop: TCheckBox;
    DefaultSourceCode: TSynEdit;
    Complet: TSynCompletionProposal;
    PasEdit: TSynEdit;
    TabToSpace: TCheckBox;
    MaxWindow: TCheckBox;
    SmartTabs: TCheckBox;
    ShowGutter: TCheckBox;
    HelpBtn: TBitBtn;
    ExeInDir: TCheckBox;
    NoOpen: TCheckBox;
    PasSyn: TSynPasSyn;
    SaveDesk: TCheckBox;
    procedure OkBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure DefaultBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RemoveDevClick(Sender: TObject);
    procedure ResetDirsClick(Sender: TObject);
    procedure CreateIniClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FontChange(Sender: TObject);
    procedure ElementListClick(Sender: TObject);
    procedure ColorGridChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BoldClick(Sender: TObject);
    procedure RemovePasFile(Sender: TObject);
    procedure TestBtnClick(Sender: TObject);
    procedure AutoIndentClick(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure EditorColorChange(Sender: TObject);
  private
  IniFileName : string;
  IniFile : TIniFile;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  EnvirForm: TEnvirForm;

Const
MaxEditorColours = 15;

Var
EditClrNames : Array [0..MaxEditorColours] of String =
(
'White',
'Yellow',
'Blue',
'Aqua',
'Navy',
'Green',
'Olive',
'Purple',
'Gray',
'Teal',
'Silver',
'Lime',
'Fuchsia',
'Black',
'Maroon',
'Red'
);

Var
EditClrInt : Array [0..MaxEditorColours] of TColorRef =
(
clWhite,
clYellow,
clBlue,
clAqua,
clNavy,
clGreen,
clOlive,
clPurple,
clGray,
clTeal,
clSilver,
clLime,
clFuchsia,
clBlack,
clMaroon,
clRed
);

Function ReturnColorIndex (Const Color : String) : TColorRef;

implementation

uses Main, MDIEdit, Utils, Version;

{$R *.DFM}

Function ReturnColorIndex (Const Color : String) : TColorRef;
Var
i : Integer;
Begin
  Result := 0;
  If Color <> '' then
  For i := 0 to MaxEditorColours
  do begin
      If UpperCase (Color) = UpperCase(EditClrNames [i])
      then begin
         Result := i;
         Break;
      end;
  end;
end;

procedure TEnvirForm.OkBtnClick(Sender: TObject);
var
  i, TrueSize : integer;

   function MakeCommaText(Fore, Back: Integer; Bold, Italic, Underline:Boolean): string;
   begin
      Result:= Format('%d, %d, %d, %d, %d', [Fore, Back, Ord(Bold), Ord(Italic), Ord(Underline)]);
   end;
begin
  TrueSize := StrToInt(Size.Text);
  if (TrueSize > 100) or (TrueSize < 4) then
     begin
          MessageDlg('Size must be between 4 and 100',MtError,[MbOK],0);
          Exit;
     end;

  if DefaultDir.Text <> '' then
     with MainForm do begin
       OpenFileDialog.InitialDir := DefaultDir.Text;
       OpenAddDialog.InitialDir  := DefaultDir.Text;
       SaveProject.InitialDir  := DefaultDir.Text;
       ExportDialog.InitialDir := DefaultDir.Text;
    end;

  { Write to the ini file the user's desired environment options }

with IniFile do begin
  WriteString('Options','DefaultDir',Defaultdir.text);
  if EditorColor.Text<>'' then
     WriteString('Options','Color', EditorColor.Text);

  WriteBool('Options','SaveDesktop', SaveDesk.Checked);
  WriteBool('Options','SavePos', SavePos.Checked);
  WriteBool('Options','SaveEditor', SaveEditor.Checked);
  WriteString('Options','Font', Font.Text);
  WriteInteger('Options','FontSize', StrToInt(Size.Text));
  WriteBool('Options','Backup',Backup.Checked);
  WriteBool('Options','Minimize',Minimize.Checked);
  WriteBool('Options','Arrange',AutoArrange.Checked);
  WriteBool('Options','LineNumbers',LineNumbers.Checked);
  WriteBool('Options','ShowScrollHint',ShowScrollHint.Checked);
  WriteBool('Options','DontShowScroll',DontShowScroll.Checked);
  WriteBool('Options','NoProjExpl',NoProjExpl.Checked);
  WriteBool('Options','ToolbarSave',ToolbarSave.Checked);
  WriteBool('Options','CompileStayOnTop', CompileStayOnTop.Checked);
  WriteBool('Options','MaxWindow', MaxWindow.Checked);
  WriteBool('Options','NoOpen', NoOpen.Checked);

  WriteBool('Options','ExecParam',ExecParam.Checked);
  WriteString('Options','Params', Params.Text);
  WriteBool('Options','AutoIndent',AutoIndent.Checked);
  WriteBool('Options','TabsToSpaces',TabToSpace.Checked);
  WriteBool('Options','ShowGutter',ShowGutter.Checked);
  WriteBool('Options','SmartTabs',SmartTabs.Checked);
  WriteBool('Options','ExeInDir',ExeInDir.Checked);

  try
    Memo.Lines.SaveToFile(MainForm.BinDir+COMPLETION_FILE);
  except
    MessageDlg('Could not save Code Completion list file.', mtError, [mbOK], 0);
  end;

  try
    DefaultSourceCode.Lines.SaveToFile(MainForm.BinDir+DEFAULTCODE_FILE)
  except
    MessageDlg('Could not save Default code file.', mtError, [mbOK], 0);
  end;

  WriteInteger('Options','TabIndent',TabIndent.Value);

  with PasSyn do
        begin
           WriteString('EditorColor','CommentAttri',
                        MakeCommaText(CommentAttri.Foreground, CommentAttri.Background,
                                      fsBold in CommentAttri.Style, fsItalic in CommentAttri.Style,
                                      fsUnderline in CommentAttri.Style ));
           WriteString('EditorColor','IdentifierAttri',
                        MakeCommaText(IdentifierAttri.Foreground, IdentifierAttri.Background,
                                      fsBold in IdentifierAttri.Style, fsItalic in IdentifierAttri.Style,
                                      fsUnderline in IdentifierAttri.Style ));
           WriteString('EditorColor','KeyAttri',
                        MakeCommaText(KeyAttri.Foreground, KeyAttri.Background,
                                      fsBold in KeyAttri.Style, fsItalic in KeyAttri.Style,
                                      fsUnderline in KeyAttri.Style ));
           WriteString('EditorColor','NumberAttri',
                        MakeCommaText(NumberAttri.Foreground, NumberAttri.Background,
                                      fsBold in NumberAttri.Style, fsItalic in NumberAttri.Style,
                                      fsUnderline in NumberAttri.Style ));
           WriteString('EditorColor','SpaceAttri',
                        MakeCommaText(SpaceAttri.Foreground, SpaceAttri.Background,
                                      fsBold in SpaceAttri.Style, fsItalic in SpaceAttri.Style,
                                      fsUnderline in SpaceAttri.Style ));
           WriteString('EditorColor','StringAttri',
                        MakeCommaText(StringAttri.Foreground, StringAttri.Background,
                                      fsBold in StringAttri.Style, fsItalic in StringAttri.Style,
                                      fsUnderline in StringAttri.Style ));
           WriteString('EditorColor','SymbolAttri',
                        MakeCommaText(SymbolAttri.Foreground, SymbolAttri.Background,
                                      fsBold in SymbolAttri.Style, fsItalic in SymbolAttri.Style,
                                      fsUnderline in SymbolAttri.Style ));
        end;

  WriteBool('Start','DevFiles', DevFile.Checked);
  WriteBool('Start','Registry', AssignC.Checked);
end;

  with MainForm do begin
     if MDIChildCount = 0 then Exit;
     for i := 0 to MDIChildCount do
         if MDIChildren[i] is TEditForm then
        (MDIChildren[i] as TEditForm).FormActivate(Sender);
  end;
  MainForm.UpdateSyntaxColor;
end;

procedure TEnvirForm.CancelBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TEnvirForm.DefaultBtnClick(Sender: TObject);
begin
  { Set environment default options }
  LineNumbers.Checked := False;
  DontShowScroll.Checked := False;
  ShowScrollHint.Checked := True;
  DefaultDir.Text:= '';
  Params.Text := '';
  EditorColor.Text:='White';
  Font.Text := 'Courier New';
  Size.Text := '10';
  SavePos.Checked := True;
  SaveDesk.Checked := True;
  SaveEditor.Checked := False;
  Backup.Checked := false;
  AutoArrange.Checked := false;
  Minimize.Checked := True;
  TabIndent.Value := 4;
  ExecParam.Checked := False;
  NoProjExpl.Checked := False;
  DevFile.Checked := True;

  Case ThisCompiler of
    GPC: AssignC.Checked := False;
    Freepascal :   AssignC.Checked := True;
  end;

  AutoIndent.Checked := True;
  ToolBarSave.Checked := True;
  CompileStayOnTop.Checked := False;
  TabToSpace.Checked := True;
  MaxWindow.Checked := False;
  ShowGutter.Checked := True;
  SmartTabs.Checked := True;
  ExeInDir.Checked := True;
  NoOpen.Checked := False;

  MainForm.CreateNewCompletionFile;
  Memo.Lines.LoadFromFile(MainForm.BinDir+COMPLETION_FILE);

  MainForm.CreateNewDefaultFile;
  DefaultSourceCode.Lines.LoadFromFile(MainForm.BinDir+DEFAULTCODE_FILE);
end;

procedure TEnvirForm.FormActivate(Sender: TObject);
var
IniFile : TIniFile;
s : string;
i : integer;

begin
  IniFileName := ExtractFilePath(Application.Exename)+ ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);

  EditorColor.Clear;
  For i := 0 to MaxEditorColours
  do begin
      EditorColor.Items.Add(EditClrNames [i]);
  end;


  with IniFile do begin
  { Read from the ini file the user's desired environment options }

  Defaultdir.Text:=ReadString('Options','DefaultDir',Defaultdir.text);

  EditorColor.ItemIndex := 0;

  s := ReadString('Options','Color', 'White');
  EditorColor.ItemIndex := ReturnColorIndex (s);

  {
  if s = 'White' then
     EditorColor.ItemIndex := 0
  else if s = 'Yellow' then
     EditorColor.ItemIndex := 1
  else if s = 'Blue' then
     EditorColor.ItemIndex := 2
  else if s = 'Aqua' then
     EditorColor.ItemIndex := 3;
  }
  if IsWinNTor2k
  then begin
     Minimize.Visible := False;
     ExeInDir.Top := ExeinDir.Top - 13;
     ExecParam.Top := ExecParam.Top - 13;
     Params.Top := Params.Top - 13;
     ExecutionPanel.Height := ExecutionPanel.Height - 13;
  end;

  SavePos.Checked := ReadBool('Options','SavePos',SavePos.Checked);
  SaveDesk.Checked := ReadBool('Options','SaveDesktop',SaveDesk.Checked);
  SaveEditor.Checked := ReadBool('Options','SaveEditor',SaveEditor.Checked);
  Backup.Checked := ReadBool('Options','Backup',Backup.Checked);
  Minimize.Checked := ReadBool('Options','Minimize',Minimize.Checked);
  AutoArrange.Checked := ReadBool('Options','Arrange',AutoArrange.Checked);
  Font.Text := ReadString('Options','Font', Font.Text);
  Size.Text := IntToStr(ReadInteger('Options','FontSize', StrToInt(Size.Text)));
  TabIndent.Value := ReadInteger('Options','TabIndent',4);
  LineNumbers.Checked := ReadBool('Options','LineNumbers',False);
  ShowScrollHint.Checked := ReadBool('Options','ShowScrollHint',True);
  DontShowScroll.Checked := ReadBool('Options','DontShowScroll',False);
  NoProjExpl.Checked := ReadBool('Options','NoProjExpl',False);
  ToolBarSave.Checked := ReadBool('Options','ToolbarSave',True);
  CompileStayOnTop.Checked := ReadBool('Options','CompileStayOnTop', False);
  MaxWindow.Checked := ReadBool('Options','MaxWindow', False);
  NoOpen.Checked := ReadBool('Options','NoOpen', False);

  ExecParam.Checked := ReadBool('Options','ExecParam',False);
  Params.Text := ReadString('Options','Params', '');

  AutoIndent.Checked := ReadBool('Options','AutoIndent',True);
  ShowGutter.Checked := ReadBool('Options','ShowGutter',True);
  SmartTabs.Checked := ReadBool('Options','SmartTabs',True);
  ExeInDir.Checked := ReadBool('Options','ExeInDir',True);

  if AutoIndent.Checked then
     TabIndent.Enabled := False
  else TabIndent.Enabled := True;

  TabToSpace.Checked := ReadBool('Options','TabsToSpaces',True);

  DevFile.Checked := ReadBool('Start','DevFiles', DevFile.Checked);
  AssignC.Checked := ReadBool('Start','Registry', AssignC.Checked);

  with PasSyn do begin
       CommentAttri.Background := clWhite;
       CommentAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','CommentAttri',''), 0, clBlack);

       IdentifierAttri.Background := clWhite;
       IdentifierAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','IdentifierAttri',''), 0, clBlack);

       KeyAttri.Background := clWhite;
       KeyAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','KeyAttri',''), 0, clBlack);

       NumberAttri.Background := clWhite;
       NumberAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','NumberAttri',''), 0, clBlack);

       SpaceAttri.Background := clWhite;
       SpaceAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','SpaceAttri',''), 0, clBlack);

       StringAttri.Background := clWhite;
       StringAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','StringAttri',''), 0, clBlack);

       SymbolAttri.Background := clWhite;
       SymbolAttri.Foreground :=
              MakeCommaTextToColor(ReadString('EditorColor','SymbolAttri',''), 0, clBlack);
  end;
  end;

  if not FileExists(MainForm.BinDir+COMPLETION_FILE) then
     MainForm.CreateNewCompletionFile;
  Memo.Lines.LoadFromFile(MainForm.BinDir+COMPLETION_FILE);


  if not FileExists(MainForm.BinDir+DEFAULTCODE_FILE) then
     MainForm.CreateNewDefaultFile;
  DefaultSourceCode.Lines.LoadFromFile(MainForm.BinDir+DEFAULTCODE_FILE)
end;

procedure TEnvirForm.RemoveDevClick(Sender: TObject);
var Reg : TRegistry;
begin
Reg := TRegistry.Create;
   with Reg do
     begin
       RootKey := HKEY_CLASSES_ROOT;
       DeleteKey('.dp');
       DeleteKey('DevPas.dp');
       Free;
     end;
end;

procedure TEnvirForm.ResetDirsClick(Sender: TObject);
var IniFile : TIniFile;
begin
  IniFileName := ExtractFilePath(Application.Exename)+ ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);

  with IniFile do begin
    If ThisCompiler = GPC
    then begin
       WriteString('Directories','BinDir',ExtractFilePath(Application.ExeName)+'\');
       WriteString('Directories','PasDir',ExtractFilePath(Application.ExeName)+'..\units\');
       WriteString('Directories','LibDir',ExtractFilePath(Application.ExeName)+'..\lib\');
    end else begin
       WriteString('Directories','BinDir',ExtractFilePath(Application.ExeName)+'Bin\');
       WriteString('Directories','PasDir',ExtractFilePath(Application.ExeName)+'Units\');
       WriteString('Directories','LibDir',ExtractFilePath(Application.ExeName)+'Units\');
    end;
  end;
end;

procedure TEnvirForm.CreateIniClick(Sender: TObject);
begin
  MainForm.CreateNewIniFile(false);
end;

procedure TEnvirForm.FormCreate(Sender: TObject);
begin
  IniFileName := ExtractFilePath(Application.Exename)+ ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);
  Font.Items.AddStrings(Screen.Fonts);
end;

procedure TEnvirForm.FontChange(Sender: TObject);
begin
  if Font.ItemIndex = 4 then
     Font.ItemIndex := 3;
end;

procedure TEnvirForm.ElementListClick(Sender: TObject);
begin
with IniFile do begin
  case ElementList.ItemIndex of
      0 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Comment',4);
      1 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Identifier',0);
      2 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Key',0);
      3 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Number',5);
      4 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Space',15);
      5 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','String',9);
      6 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Symbol',0);
      7 : ColorGrid.ForegroundIndex:= ReadInteger('Colors','Color',0);
  end;
end;
end;

procedure TEnvirForm.ColorGridChange(Sender: TObject);
begin
with IniFile do begin
   case ElementList.ItemIndex of
      0:
      begin
         PasSyn.CommentAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Comment',ColorGrid.ForegroundIndex);
      end;
      1:
      begin
         PasSyn.IdentifierAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Identifier',ColorGrid.ForegroundIndex);
      end;
      2:
      begin
         PasSyn.KeyAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Key',ColorGrid.ForegroundIndex);
      end;
      3:
      begin
         PasSyn.NumberAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Number',ColorGrid.ForegroundIndex);
      end;
      4:
      begin
         PasSyn.SpaceAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Space',ColorGrid.ForegroundIndex);
      end;
      5:
      begin
         PasSyn.StringAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','String',ColorGrid.ForegroundIndex);
      end;
      6:
      begin
         PasSyn.SymbolAttri.Foreground:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Symbol',ColorGrid.ForegroundIndex);
      end;
      7:
      begin
         PasEdit.Color:= clWhite;
         PasEdit.Font.Color:= ColorGrid.ForegroundColor;
         WriteInteger('Colors','Color',ColorGrid.ForegroundIndex);
      end;
   end;
 end;
end;

procedure TEnvirForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IniFile.Free;
end;

procedure TEnvirForm.BoldClick(Sender: TObject);
var
  chkBox: TCheckBox;
begin
   chkBox:= TCheckBox(Sender);
   case ElementList.ItemIndex of
      0:
      begin
         with PasSyn.CommentAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      1:
      begin
         with PasSyn.IdentifierAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      2:
      begin
         with PasSyn.KeyAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      3:
      begin
         with PasSyn.NumberAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      4:
      begin
         with PasSyn.SpaceAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      5:
      begin
         with PasSyn.StringAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
      6:
      begin
         with PasSyn.SymbolAttri do
         begin
            case chkBox.Tag of
                0: if chkBox.Checked then  Style:= Style + [fsBold]
                   else Style:= Style - [fsBold];
                1: if chkBox.Checked then  Style:= Style + [fsItalic]
                   else Style:= Style - [fsItalic];
                2: if chkBox.Checked then  Style:= Style + [fsUnderline]
                   else Style:= Style - [fsUnderline];
              end;
         end;
      end;
   end;
end;

procedure TEnvirForm.RemovePasFile(Sender: TObject);
var Reg : TRegistry;
begin
Reg := TRegistry.Create;
   with Reg do
     begin
       RootKey := HKEY_CLASSES_ROOT;
       DeleteKey('.pas');
       DeleteKey('DevPas.pas');
       DeleteKey('.pp');
       DeleteKey('DevPas.pp');
       Free;
     end;
end;

procedure TEnvirForm.TestBtnClick(Sender: TObject);
begin
  Complet.ItemList.Assign(Memo.Lines);
  Complet.Execute('', TestBtn.Left+TestBtn.Width, TestBtn.Top+TestBtn.Height);
end;

procedure TEnvirForm.AutoIndentClick(Sender: TObject);
begin
  if AutoIndent.Checked then
     TabIndent.Enabled := False
  else TabIndent.Enabled := True;
end;

procedure TEnvirForm.HelpBtnClick(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\DevPas.hlp';
  Application.HelpJump('EnvironmentOptions');
end;

procedure TEnvirForm.EditorColorChange(Sender: TObject);
Var
NewColor : String;
begin
  With EditForm
  do begin
     NewColor := EditorColor.Text;
     if NewColor <> ''
     then begin
        IniFile.WriteString ('Options','Color', NewColor);
        MainForm.UpdateSyntaxColor;
     end;
  end; // with EditForm
end;

end.
