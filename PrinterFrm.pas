unit PrinterFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, marsCap, SynEditPrint;

type
  TPrinter = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    pHeader: TCheckBox;
    Label1: TLabel;
    pCopies: TSpinEdit;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    pUnit: TLabel;
    pLineNumber: TCheckBox;
    pWrapLines: TCheckBox;
    Label6: TLabel;
    FromPage: TSpinEdit;
    Label7: TLabel;
    ToPage: TSpinEdit;
    PreviewBtn: TBitBtn;
    PrintEdit: TSynEditPrint;
    pLineNumberMargin: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure PageChange(Sender: TObject);
    procedure PreviewBtnClick(Sender: TObject);
    procedure SetPrintEditConfig;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Printer: TPrinter;

implementation

uses PrintPreviewFrm, Main, MdiEdit;

{$R *.DFM}

procedure TPrinter.FormActivate(Sender: TObject);
begin
 if FromPage.Value = 0 then
    FromPage.Value := 1;
 if ToPage.Value = 0 then
    ToPage.Value := 1;
end;

procedure TPrinter.PageChange(Sender: TObject);
begin
 if (Sender as TSpinEdit).Value <= 0 then
    (Sender as TSpinEdit).Value := 1;
end;

procedure TPrinter.PreviewBtnClick(Sender: TObject);
begin
  try
    PrintPreviewForm := TPrintPreviewForm.Create(self);
    SetPrintEditConfig;
    PrintPreviewForm.PrintPreview.SynEditPrint := PrintEdit;

    PrintPreviewForm.ShowModal;
  finally
    PrintPreviewForm.Free;
  end;
end;

procedure TPrinter.SetPrintEditConfig;
begin
  with PrintEdit do begin
      if pHeader.Checked then begin
         Footer.Clear;
         Header.Clear;

         Header.Add('', Header.DefaultFont, taLeftJustify, 1);
         Header.Add('', Header.DefaultFont, taLeftJustify, 2);
         Header.Add('$TITLE$', Header.DefaultFont, taLeftJustify, 3);
         Header.Add('$DATETIME$', Header.DefaultFont, taRightJustify, 3);
         Header.Add('', Header.DefaultFont, taLeftJustify, 4);

         Footer.Add('', Footer.DefaultFont, taLeftJustify, 1);
         Footer.Add('$PAGENUM$/$PAGECOUNT$', Footer.DefaultFont, taCenter, 2);
         Footer.Add('', Footer.DefaultFont, taLeftJustify, 3);

         Title := pUnit.Caption;
      end;

      LineNumbers := pLineNumber.Checked;
      LineNumbersInMargin := pLineNumberMargin.Checked;
      Wrap := pWrapLines.Checked;
      Highlight := True;
  end;
end;

end.
