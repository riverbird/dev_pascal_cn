unit TemplateFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Spin, ExtDlgs, Buttons, StdCtrls, ComCtrls, ToolWin, Menus, ExtCtrls,
  Clipbrd, IniFiles, ImgList;

type
  TTemplateForm = class(TForm)
    MainMenu: TMainMenu;
    ToolBar1: TToolBar;
    PageControl1: TPageControl;
    StatusBar1: TStatusBar;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PasCode: TRichEdit;
    NewBtn: TToolButton;
    OpenBtn: TToolButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    NameEdit: TEdit;
    Label2: TLabel;
    Description: TEdit;
    Label3: TLabel;
    Catagory: TEdit;
    Label4: TLabel;
    IconFN: TEdit;
    SpeedButton1: TSpeedButton;
    OpenPictureDialog1: TOpenPictureDialog;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    SpinEdit1: TSpinEdit;
    Label6: TLabel;
    SpinEdit2: TSpinEdit;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    Param: TEdit;
    Label8: TLabel;
    ProjectName: TEdit;
    Panel1: TPanel;
    Image1: TImage;
    Label9: TLabel;
    File1: TMenuItem;
    Edit1: TMenuItem;
    Undo1: TMenuItem;
    N1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Cut1: TMenuItem;
    Delete1: TMenuItem;
    N2: TMenuItem;
    SelectAll1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    N3: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    HelpContents1: TMenuItem;
    N4: TMenuItem;
    About1: TMenuItem;
    N5: TMenuItem;
    NewWindow1: TMenuItem;
    SaveBtn: TToolButton;
    ExitBtn: TToolButton;
    ToolButton1: TToolButton;
    Image2: TImage;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    NewWindowBtn: TToolButton;
    ToolButton3: TToolButton;
    ImageList1: TImageList;
    Panel2: TPanel;
    PopupMenu1: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem7: TMenuItem;
    Undo2: TMenuItem;
    Cut2: TMenuItem;
    Copy2: TMenuItem;
    Paste2: TMenuItem;
    Delete2: TMenuItem;
    SelectAll2: TMenuItem;
    Label14: TLabel;
    CompilerOpt: TEdit;
    GroupBox4: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox3: TCheckBox;
    IncDir: TEdit;
    Label15: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpinEdit4Change(Sender: TObject);
    procedure IconFNChange(Sender: TObject);
    procedure Undo1Click(Sender: TObject);
    procedure Cut1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure NewWindow1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure CheckBoxClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Open1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure HelpClick(Sender: TObject);
    procedure AppHint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FFileName: String;
    Function GetFileName: String;
    procedure SetFileName(Value: String);
  public
    Modified: Boolean;
    procedure Open(Const AFileName: String);
    procedure Save(AFileName: String);
    property FileName: String read GetFileName write SetFileName;
  end;

var
  TemplateForm: TTemplateForm;

implementation

uses
  Main;

{$R *.DFM}

Const
  sUntitled = 'Untitled';
  sAppName = 'Template Builder';
  sStdCode =
    'program MyTemplate;'+#13#10+
    'begin'+#13#10+
    ''+#13#10+
    '{ Put your Pascal codes here }'+#13#10+
    ''+#13#10+
    'end.'+#13#10;

procedure TTemplateForm.SpeedButton1Click(Sender: TObject);
begin
  if OpenPictureDialog1.Execute then
     IconFN.Text := OpenPictureDialog1.FileName;
end;

procedure TTemplateForm.SpinEdit4Change(Sender: TObject);
begin
  if (length(TSpinEdit(Sender).Text) > 0) and (TSpinEdit(Sender).Value < 0) then
     TSpinEdit(Sender).Value := 0;
     
  EditChange(Sender);
end;

procedure TTemplateForm.IconFNChange(Sender: TObject);
begin
  {$IFDEF Win32}
  if CompareText(Copy(IconFN.Text, 1, 2), 'A:') = 0 then Exit;
  {$ENDIF}

  if (FileExists(IconFN.Text)) or
     (FileExists(ExtractFilePath(FileName) + IconFN.Text)) then
     try
        Image1.Picture.LoadFromFile(IconFN.Text);
     except
        Image1.Picture.Bitmap.FreeImage;
     end
  else
     Image1.Picture.Bitmap.FreeImage;
  EditChange(Sender);
end;

procedure TTemplateForm.Undo1Click(Sender: TObject);
begin
  ActiveControl.Perform(EM_UNDO, 0, 0);
end;

procedure TTemplateForm.Cut1Click(Sender: TObject);
begin
  TCustomEdit(ActiveControl).CutToClipboard;
end;

procedure TTemplateForm.Copy1Click(Sender: TObject);
begin
  TCustomEdit(ActiveControl).CopyToClipboard;
end;

procedure TTemplateForm.Paste1Click(Sender: TObject);
begin
  TCustomEdit(ActiveControl).PasteFromClipboard;
end;

procedure TTemplateForm.Delete1Click(Sender: TObject);
begin
  TCustomEdit(ActiveControl).ClearSelection;
end;

procedure TTemplateForm.SelectAll1Click(Sender: TObject);
begin
  TCustomEdit(ActiveControl).SelectAll;
end;

procedure TTemplateForm.Edit1Click(Sender: TObject);
var
  E: Boolean;
begin
  E := ActiveControl is TCustomEdit;
  Undo1.Enabled := E;
  Cut1.Enabled := E;
  Copy1.Enabled := E;
  Paste1.Enabled := E;
  Delete1.Enabled := E;
  SelectAll1.Enabled := E;
  if E then
  begin
     Undo1.Enabled := TCustomEdit(ActiveControl).Perform(EM_CANUNDO, 0, 0) = 1;
     Cut1.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Copy1.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Delete1.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Paste1.Enabled := Clipboard.HasFormat(CF_TEXT);
  end;
end;

procedure TTemplateForm.NewWindow1Click(Sender: TObject);
begin
  TemplateForm := TTemplateForm.Create(MainForm);
  TemplateForm.Show;
end;

procedure TTemplateForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TTemplateForm.EditChange(Sender: TObject);
begin
  Modified := True;
end;

procedure TTemplateForm.CheckBoxClick(Sender: TObject);
begin
  Modified := True;
end;

procedure TTemplateForm.FormCreate(Sender: TObject);
begin
  Modified := False;
end;

procedure TTemplateForm.AppHint(Sender: TObject);
begin
  StatusBar1.Panels.Items[0].Text := GetLongHint(Application.Hint);
end;


procedure TTemplateForm.Open(Const AFileName: String);
var
  Ini: TIniFile;
  F: String;
begin
  Ini := TIniFile.Create(AFileName);
  NameEdit.Text := Ini.ReadString('Template', 'Name', 'My Template');
  IconFN.Text   := Ini.ReadString('Template', 'Icon', '');
  Description.Text := Ini.ReadString('Template', 'Description',
    'A simple template.');
  Catagory.Text := Ini.ReadString('Template', 'Catagory', 'Additional');

  PasCode.Lines.Text := sStdCode;

  F := Ini.ReadString('Editor', 'Text',
    ChangeFileExt(ExtractFileName(AFileName), '.txt'));
  if FileExists(F) then PasCode.Lines.LoadFromFile(F);

  SpinEdit1.Value := Ini.ReadInteger('Editor', 'CursorX', 0);
  SpinEdit2.Value := Ini.ReadInteger('Editor', 'CursorY', 0);
  CheckBox1.Checked := Ini.ReadBool('Project', 'Console', True);
  CheckBox3.Checked := Ini.ReadBool('Project', 'DLL', False);
  CompilerOpt.Text := Ini.ReadString('Project', 'CompilerOptions', '');
  IncDir.Text := Ini.ReadString('Project', 'IncludeDirs', '');
  Param.Text := Ini.ReadString('Project', 'Libs', '');
  ProjectName.Text := Ini.ReadString('Project', 'Name', 'Project1');

  Ini.Free;
  FileName := AFileName;
end;

procedure TTemplateForm.Save(AFileName: String);
var
  Ini : TIniFile;
begin
  if UpperCase(ExtractFileExt(AFileName)) = '.TEM' then
     AFileName := ChangeFileExt(AFileName, '.template');

  Ini := TIniFile.Create(AFileName);
  Ini.WriteString('Template', 'Name', NameEdit.Text);
  Ini.WriteString('Template', 'Icon', ExtractFileName(IconFN.Text));
  Ini.WriteString('Template', 'Description', Description.Text);
  Ini.WriteString('Template', 'Catagory', Catagory.Text);

  Ini.WriteString('Editor', 'Text', ChangeFileExt(ExtractFileName(AFileName),
       '.txt'));

  Ini.WriteInteger('Editor', 'CursorX', SpinEdit1.Value);
  Ini.WriteInteger('Editor', 'CursorY', SpinEdit2.Value);
  Ini.WriteBool('Project', 'Console', CheckBox1.Checked);
  Ini.WriteBool('Project', 'DLL', CheckBox3.Checked);
  Ini.WriteString('Project', 'CompilerOptions', CompilerOpt.Text);
  Ini.WriteString('Project', 'IncludeDirs', IncDir.Text);
  Ini.WriteString('Project', 'Libs', Param.Text);
  Ini.WriteString('Project', 'Name', ProjectName.Text);

  Ini.Free;
  CopyFile(PChar(IconFN.Text), PChar(ExtractFilePath(AFileName) +
    ExtractFileName(IconFN.Text)), False);
  if ExtractFileName(IconFN.Text) <> '' then
     IconFN.Text := ExtractFileName(IconFN.Text);
  PasCode.Lines.SaveToFile(ChangeFileExt(ExtractFileName(AFileName), '.txt'));
  FileName := AFileName;
end;

Function TTemplateForm.GetFileName: String;
begin
  Result := FFileName;
end;

procedure TTemplateForm.SetFileName(Value: String);
begin
  FFileName := Value;
  Modified := False;
  if Value = '' then
     Caption := sAppName + ' - ' + sUntitled
  else
     Caption := sAppName + ' - ' + ExtractFileName(Value);
end;

procedure TTemplateForm.PopupMenu1Popup(Sender: TObject);
var
  E: Boolean;
begin
  E := ActiveControl is TCustomEdit;
  Undo2.Enabled := E;
  Cut2.Enabled := E;
  Copy2.Enabled := E;
  Paste2.Enabled := E;
  Delete2.Enabled := E;
  SelectAll2.Enabled := E;
  if E then
  begin
     Undo2.Enabled := TCustomEdit(ActiveControl).Perform(EM_CANUNDO, 0, 0) = 1;
     Cut2.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Copy2.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Delete2.Enabled := TCustomEdit(ActiveControl).SelLength > 0;
     Paste2.Enabled := Clipboard.HasFormat(CF_TEXT);
  end;
end;

procedure TTemplateForm.Save1Click(Sender: TObject);
begin
  SaveDialog1.InitialDir := ExtractFilePath(Application.ExeName)+'Templates\';
  if FileExists(FileName) then
     Save(FileName)
  else
     SaveAs1Click(Sender);
end;

procedure TTemplateForm.SaveAs1Click(Sender: TObject);
{var
  Ini : TIniFile;
  j   : integer;}
begin
  if SaveDialog1.Execute then begin
     Save(SaveDialog1.FileName);
     {Ini := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Templates\Index.ini');

     j := Ini.ReadInteger('Index','Count',0);
     inc(j);
     Ini.WriteInteger('Index','Count',j);
     Ini.WriteString('Index','File'+IntToStr(j),ExtractFileName(FileName));

     Ini.Free;}
  end;
end;

procedure TTemplateForm.New1Click(Sender: TObject);
var
  i: Integer;
begin
  if Modified then
  begin
     i := Application.MessageBox('This template has been modified.' + #13#10 +
       'Do you want to save it?', 'Confirm', MB_ICONQUESTION + MB_YESNOCANCEL);
     if i = 6 then
     begin
        Save1Click(Sender);
        if Modified then Exit;
     end else if i <> 7 then
        Exit;
  end;

  FileName := '';
  NameEdit.Text := 'My Template';
  Description.Text := 'A simple template.';
  Catagory.Text := 'Additional';
  IconFN.Text := '';
  SpinEdit1.Value := 0;
  SpinEdit2.Value := 0;
  ProjectName.Text := 'Project1';
  Param.Text := '';
  CheckBox1.Checked := True;


  PasCode.Lines.Text := sStdCode;
end;

procedure TTemplateForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  i: Integer;
begin
  if Modified then
  begin
     i := Application.MessageBox('This template has been modified.' + #13#10 +
       'Do you want to save it?', 'Confirm', MB_ICONQUESTION + MB_YESNOCANCEL);
     if i = 6 then
     begin
        Save1Click(Sender);
        if Modified then CanClose := False;
        CanClose := True;
     end else if i = 7 then
        CanClose := True
     else
        CanClose := False;
  end else
     CanClose := True;
end;

procedure TTemplateForm.Open1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
     Open(OpenDialog1.FileName);
end;

procedure TTemplateForm.About1Click(Sender: TObject);
begin
  Application.MessageBox('Template Builder version 1.0.1 for Dev-Pascal, by Hongli Lai' +
    #13#10 + 'Copyright (c) 2000 Hongli Lai' + #13#10 +
    'Covered by the GNU General Public License.' + #13#10#13#10 +
    'This program is free software; see the source for copying conditions.' + #13#10 +
    'There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A' + #13#10 +
    'PARTICULAR PURPOSE.', 'About', MB_ICONASTERISK);
end;

procedure TTemplateForm.HelpClick(Sender: TObject);
begin
  Application.HelpFile := ExtractFilePath(ParamStr(0))+'Help\DevPas.hlp';
  Application.HelpJump('UsingTemplates');
end;

procedure TTemplateForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
