unit ResEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Menus, ComCtrls, ExtCtrls, marsCap, Clipbrd, TB97,
  ExtDlgs, Project, SynEditHighlighter, SynEdit, SynHighlighterCPP,
  ImgList, TB97Tlbr, TB97Ctls;

type
  TResEditForm = class(TForm)
    GroupBox1: TGroupBox;
    PopupMenu: TPopupMenu;
    Undo2: TMenuItem;
    N10: TMenuItem;
    Cut2: TMenuItem;
    Copy2: TMenuItem;
    Paste2: TMenuItem;
    N9: TMenuItem;
    SelectAll2: TMenuItem;
    a: TLabel;
    b: TLabel;
    c: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Dock971: TDock97;
    EditBar: TToolbar97;
    Dock972: TDock97;
    ClearBtn: TToolbarButton97;
    CutBtn: TToolbarButton97;
    CopyBtn: TToolbarButton97;
    PasteBtn: TToolbarButton97;
    ToolbarSep971: TToolbarSep97;
    MMResBar: TToolbar97;
    BitmapBtn: TToolbarButton97;
    IconBtn: TToolbarButton97;
    ToolbarButton971: TToolbarButton97;
    UndoBtn: TToolbarButton97;
    ToolbarSep972: TToolbarSep97;
    OpenDialog: TOpenPictureDialog;
    Dock973: TDock97;
    Dock974: TDock97;
    ToolbarSep973: TToolbarSep97;
    InsertFilenameBtn: TToolbarButton97;
    OpenFileDialog: TOpenDialog;
    OpenBtn: TToolbarButton97;
    MenuBtn: TToolbarButton97;
    Minimize: TCheckBox;
    ResEditor: TSynEdit;
    CppSyn: TSynCppSyn;
    SaveDialog: TSaveDialog;
    SaveBtn: TToolbarButton97;
    ToolbarSep974: TToolbarSep97;
    CloseBtn: TBitBtn;
    BuildResBtn: TBitBtn;
    ImageList_Default: TImageList;
    ImageList_Gnome: TImageList;
    DialogBtn: TToolbarButton97;
    procedure Undo2Click(Sender: TObject);
    procedure Cut2Click(Sender: TObject);
    procedure Copy2Click(Sender: TObject);
    procedure Paste2Click(Sender: TObject);
    procedure SelectAll2Click(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure ToolBarClose(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
    procedure BitmapBtnClick(Sender: TObject);
    procedure IconBtnClick(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure ResEditorChange(Sender: TObject);
    procedure InsertFilenameBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OpenBtnClick(Sender: TObject);
    procedure MenuBtnClick(Sender: TObject);
    procedure SaveResourceFile(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure CloseBtnClick(Sender: TObject);
    procedure MinimizeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BuildResBtnClick(Sender: TObject);
    procedure DialogBtnClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    PathName : string;
    { Déclarations publiques }
  end;

var
  ResEditForm: TResEditForm;

procedure EditResource(Owner: TComponent; FileName: string);

implementation

uses
  UResCodeEdit, Main, MenuFrm, Utils, CreateDialogFrm;

const
  FormCaption = 'Resource file editor';

{$R *.DFM}

{$ifdef ver100}
   {$define Old_Delphi}
{$endif}

{$ifdef ver120}
   {$define Old_Delphi}
{$endif}

procedure EditResource(Owner: TComponent; FileName: string);
begin
  ResEditForm := TResEditForm.Create(Owner);
  with ResEditForm do begin
     if FileName <> '' then
        ResEditor.Lines.LoadFromFile(FileName);

     PathName := FileName;
     if PathName <> '' then
        Caption := FormCaption + ' - ' + PathName;
     Show;
  end;
end;

procedure TResEditForm.Undo2Click(Sender: TObject);
begin
  ResEditor.Undo;
end;

procedure TResEditForm.Cut2Click(Sender: TObject);
begin
  ResEditor.CutToClipboard;
end;

procedure TResEditForm.Copy2Click(Sender: TObject);
begin
  ResEditor.CopyToClipboard;
end;

procedure TResEditForm.Paste2Click(Sender: TObject);
begin
  ResEditor.PasteFromClipboard;
end;

procedure TResEditForm.SelectAll2Click(Sender: TObject);
begin
  ResEditor.Selectall;
end;

procedure TResEditForm.PopupMenuPopup(Sender: TObject);
begin
  Cut2.Enabled := ResEditor.SelAvail;
  Copy2.Enabled := ResEditor.SelAvail;
  Paste2.Enabled := Clipboard.HasFormat(CF_TEXT);
end;

procedure TResEditForm.ToolBarClose(Sender: TObject);
begin
  with TToolbar97(Sender) do begin
     Show;
     DockedTo := DefaultDock;
  end;
end;

procedure TResEditForm.ClearBtnClick(Sender: TObject);
begin
  if Application.MessageBox('Are you sure you want to start a new file ?', 'Clear document', MB_YESNO+MB_ICONEXCLAMATION) = 6 then begin
     ResEditor.Lines.Clear;
     PathName := '';
     Caption := FormCaption;
  end;
end;

procedure TResEditForm.BitmapBtnClick(Sender: TObject);
var
  S: String;
  FileN : string;
  j : integer;
begin
  ResCodeEditForm := TResCodeEditForm.Create(Self);
  with ResCodeEditForm do
  try
     PictureDialog := True;
     Caption := FormCaption + '(bitmap)';
     MemOption.ItemIndex := 1;
     ShowModal;
     if ResCodeEditForm.ModalResult <> mrAbort then begin // strangely, putting mrOK this doesn't work, althougth it has been correctly setted in RedCodeEditForm
        S := ResID.Text + ' BITMAP';
        if CheckBox1.Checked then begin
           case LoadOption.ItemIndex of
            1: S := S + ' PRELOAD';
            2: S := S + ' LOADONCALL';
           end;
           case MemOption.ItemIndex of
            1: S := S + ' MOVEABLE';
            2: S := S + ' DISCARDABLE';
           end;
        end;
        FileN := FileName.Text;
        while Pos('\', FileN)<>0 do begin
          j := Pos('\',FileN);
          Delete(FileN,j,1);
          Insert('/', FileN, j);
        end;
        if FileN[1] <> '"' then
           S := S + ' "' + FileN + '"'
        else S := S + ' ' + FileN;

        ResEditor.SelText := S;
     end;
  finally
     Free;
  end;
end;

procedure TResEditForm.IconBtnClick(Sender: TObject);
var
  S: String;
  FileN : string;
  j : integer;
begin
  ResCodeEditForm := TResCodeEditForm.Create(Self);
  with ResCodeEditForm do
  try
     PictureDialog := True;
     Caption := FormCaption + '(icon)';
     MemOption.ItemIndex := 1;
     OpenPictureDialog1.FilterIndex := 2;
     ShowModal;
     if ResCodeEditForm.ModalResult <> mrAbort then begin // strangely, putting mrOK this doesn't work, althougth it has been correctly setted in RedCodeEditForm
        S := ResID.Text + ' ICON';
        if CheckBox1.Checked then begin
           case LoadOption.ItemIndex of
            1: S := S + ' PRELOAD';
            2: S := S + ' LOADONCALL';
           end;
           case MemOption.ItemIndex of
            1: S := S + ' MOVEABLE';
            2: S := S + ' DISCARDABLE';
           end;
        end;
        FileN := FileName.Text;
        while Pos('\', FileN)<>0 do begin
          j := Pos('\',FileN);
          Delete(FileN,j,1);
          Insert('/', FileN, j);
        end;
        if FileN[1] <> '"' then
           S := S + ' "' + FileN + '"'
        else S := S + ' ' + FileN;
        ResEditor.SelText := S;
     end;
  finally
     Free;
  end;
end;

procedure TResEditForm.ToolbarButton971Click(Sender: TObject);
var
  S: String;
  FileN : string;
  j : integer;
begin
  ResCodeEditForm := TResCodeEditForm.Create(Self);
  with ResCodeEditForm do
  try
     Caption := FormCaption + '(font)';
     MemOption.ItemIndex := 1;
     ShowModal;
     if ResCodeEditForm.ModalResult <> mrAbort then begin // strangely, putting mrOK this doesn't work, althougth it has been correctly setted in RedCodeEditForm
        S := ResID.Text + ' FONT';
        if CheckBox1.Checked then begin
           case LoadOption.ItemIndex of
            1: S := S + ' PRELOAD';
            2: S := S + ' LOADONCALL';
           end;
           case MemOption.ItemIndex of
            1: S := S + ' MOVEABLE';
            2: S := S + ' DISCARDABLE';
           end;
        end;
        FileN := FileName.Text;
        while Pos('\', FileN)<>0 do begin
          j := Pos('\',FileN);
          Delete(FileN,j,1);
          Insert('/', FileN, j);
        end;
        if FileN[1] <> '"' then
           S := S + ' "' + FileN + '"'
        else S := S + ' ' + FileN;

        ResEditor.SelText := S;
     end;
  finally
     Free;
  end;
end;

procedure TResEditForm.UndoBtnClick(Sender: TObject);
begin
  ResEditor.Undo;
//  Perform(EM_UNDO, 0, 0);
end;

procedure TResEditForm.ResEditorChange(Sender: TObject);
begin
  UndoBtn.Enabled := True;
end;

procedure TResEditForm.InsertFilenameBtnClick(Sender: TObject);
var FileN : string;
    j : integer;
begin
  if OpenDialog.Execute then begin
     FileN := OpenDialog.FileName;
     while Pos('\', FileN)<>0 do begin
       j := Pos('\',FileN);
       Delete(FileN,j,1);
       Insert('/', FileN, j);
     end;

     ResEditor.Lines.Add(FileN);
  end;
end;

procedure TResEditForm.FormCreate(Sender: TObject);
begin
  Icon.Handle := Application.Icon.Handle;
end;

procedure TResEditForm.OpenBtnClick(Sender: TObject);
begin
  if OpenFileDialog.Execute then begin
     PathName := OpenFileDialog.FileName;
     try
       ResEditor.Lines.LoadFromFile(PathName);
       Caption := FormCaption + ' - ' + PathName;
     except
       MessageDlg('Could not open file "'+PathName+'"', mtError, [mbOK], 0);
     end;
  end;
end;

procedure TResEditForm.MenuBtnClick(Sender: TObject);
var i, j : integer;
    s    : string;
begin
  MenuForm := TMenuForm.Create(self);
  with MenuForm do
  try
    if ShowModal = mrOK then begin
       s :=
       IDMenu.Text +' MENU'+#13+
       'BEGIN'+#13;

       for i := 0 to ItemCount do begin
           s := s +
           '        POPUP "'+Menu.Items[i].Text+'"'+#13+
           '        BEGIN'+#13;
           for j := 0 to Menu.Items[i].Count-1 do
               if Menu.Items[i].Item[j].Text = '-' then
                  s := s+
                  '                MENUITEM SEPARATOR'+#13
               else s := s +
                  '                MENUITEM "'+Menu.Items[i].Item[j].Text+'", '+ IntToStr(Menu.Items[i].Item[j].ImageIndex)+#13;


           s := s +
           '        END'+#13+#13;
       end;

       s := s + 'END'+#13+#13;
       ResEditor.SelText := s;
    end;
  finally
    Free;
  end;
end;

procedure TResEditForm.SaveResourceFile(Sender: TObject);
begin
  if PathName <> '' then begin
     ResEditor.Lines.SaveToFile(PathName);
     if ProjectOpen then
        MainForm.Project.ResFileModified := True;
     ResEditor.Modified := False;
  end
  else if SaveDialog.Execute then begin
     ResEditor.Lines.SaveToFile(SaveDialog.FileName);
     PathName := SaveDialog.FileName;
     Caption := FormCaption + ' - ' + PathName;
     if ProjectOpen then
        MainForm.Project.ResFileModified := True;
     ResEditor.Modified := False;
  end
  else if ProjectOpen then
      MainForm.Project.ResFileModified := False;
end;

procedure TResEditForm.FormDeactivate(Sender: TObject);
begin
  if Minimize.Checked then
     WindowState := wsMinimized;
end;

procedure TResEditForm.CloseBtnClick(Sender: TObject);
begin
{  if ResEditor.Modified then
     case MessageDlg('Do you want to save your file before closing ?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
     mrYes : begin
               SaveResourceFile(Sender);
               Close;
             end;
     mrNo  : Close;
     end;}
  Close;
end;

procedure TResEditForm.MinimizeClick(Sender: TObject);
begin
  MainForm.IniFile.WriteBool('Options','MinResEdit', Minimize.Checked);
end;

procedure TResEditForm.FormActivate(Sender: TObject);
var i : integer;
begin
  Minimize.Checked := MainForm.IniFile.ReadBool('Options','MinResEdit', False);

  if MainForm.IconStyle = isDefault then begin
     for i := 0 to EditBar.ControlCount-1 do
         if (EditBar.Controls[i] is TToolbarButton97) then
            (EditBar.Controls[i] as TToolbarButton97).Images := ImageList_Default;
     for i := 0 to MMResBar.ControlCount-1 do
         if (MMResBar.Controls[i] is TToolbarButton97) then
            (MMResBar.Controls[i] as TToolbarButton97).Images := ImageList_Default;
  end else begin
     for i := 0 to EditBar.ControlCount-1 do
         if (EditBar.Controls[i] is TToolbarButton97) then
            (EditBar.Controls[i] as TToolbarButton97).Images := ImageList_Gnome;
     for i := 0 to MMResBar.ControlCount-1 do
         if (MMResBar.Controls[i] is TToolbarButton97) then
            (MMResBar.Controls[i] as TToolbarButton97).Images := ImageList_Gnome;
  end
end;

procedure TResEditForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ResEditor.Modified then
     case MessageDlg('Do you want to save your file before closing ?', mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
     mrYes : begin
               SaveResourceFile(Sender);
               CanClose := True;
             end;
     mrNo  : CanClose := True;
     mrCancel : CanClose := False;
     end;
end;

procedure TResEditForm.BuildResBtnClick(Sender: TObject);
var ObjFile, LogParams : string;
begin
  SaveResourceFile(Sender);

  if PathName = '' then
     exit;

  MainForm.CompilerOutput.Items.Clear;
  MainForm.ResourceOutput.Items.Clear;
  MainForm.LogBox.Clear;
  {$ifndef Old_Delphi}MainForm.ResSheet.Highlighted := False;{$endif}
  MainForm.ReloadDirectories;
  MainForm.GetDirectories;

  ObjFile := ChangeFileExt(GetShortName(PathName),'.o');

  LogParams := MainForm.BinDir+'windres '+MainForm.Directories.Pas+' -i '+GetShortName(PathName)+' -o '+lowercase(ObjFile)+' --preprocessor cpp';
  MainForm.LogBox.Lines.Add('Building resource file :');
  MainForm.LogBox.Lines.Add(LogParams);

  MainForm.ResourceOutput.Items.Text := CompileFile(LogParams, MainForm.BinDir);

  if MainForm.ResourceOutput.Items.Text = '' then begin
    MainForm.AddItem('','','Resource file compiled sucessfully');
    MainForm.ResourceOutput.Items.Text := 'Resource file compiled sucessfully'
  end
  else begin
    {$ifndef Old_Delphi}MainForm.ResSheet.Highlighted := True;{$endif}
    MainForm.MessageControl.ActivePage := MainForm.ResSheet;
    ResEditForm.WindowState := wsMinimized;
  end;
end;

procedure TResEditForm.DialogBtnClick(Sender: TObject);
var s, options, dialog : string;
begin
  try
    CreateDialogForm := TCreateDialogForm.Create(self);

    with CreateDialogForm do
    if ShowModal = mrOK then begin
       if ClientEdge.Checked or StaticEdge.Checked then
          dialog := ' DIALOGEX '
       else dialog := ' DIALOG ';

       s := ID.Text + dialog +IntToStr(LeftValue.Value)+', '+IntToStr(TopValue.Value)+', '+IntToStr(HeightValue.Value)+', '+IntToStr(WidthValue.Value)+#13;

       options := 'STYLE WS_VISIBLE | ';

       if SysMenu.Checked then
          options := options + 'WS_SYSMENU | ';
       if Min.Checked then
          options := options + 'WS_MINIMIZEBOX | ';
       if Maxi.Checked then
          options := options + 'WS_MAXIMIZEBOX | ';
       if VertScroll.Checked then
          options := options + 'WS_VSCROLL | ';
       if HorizScroll.Checked then
          options := options + 'WS_HSCROLL | ';
       if Popup.Checked then
          options := options + 'WS_POPUP | ';
       if TitleBar.Checked then
          options := options + 'WS_CAPTION | ';
       if Modal.Checked then
          options := options + 'DS_MODALFRAME | ';
       if Resizable.Checked then
          options := options + 'WS_THICKFRAME | ';

       if options[length(options)-1]='|' then
          options[length(options)-1] := ' ';
       options := options + #13;

       if ClientEdge.Checked or StaticEdge.Checked then begin
          options := options + 'EXSTYLE ';
          if ClientEdge.Checked then
             options := options + 'WS_EX_CLIENTEDGE |';
          if StaticEdge.Checked then
             options := options + ' WS_EX_STATICEDGE';

          if options[length(options)]='|' then
             options[length(options)]:= ' ';
          options := options + #13;
       end;

       ResEditor.SelText := s+options+'CAPTION "'+Title.Text+'"'#13+
                            'FONT '+IntToStr(FontSize.Value)+',"'+FontName.Text+'"'+#13+
                            'BEGIN'+#13+'END';
    end;
  finally
    CreateDialogForm.Free;
  end;
end;

end.
