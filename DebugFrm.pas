unit DebugFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Utils, IniFiles, ExtCtrls;

type
  TDbgForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    ContinueBtn: TBitBtn;
    Cancel: TBitBtn;
    HelpBtn: TBitBtn;
    Msg: TCheckBox;
    Label3: TLabel;
    Bevel1: TBevel;
    procedure ContinueBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
  IniFileName: String;
    { Déclarations privées }
  public
  FileName : string;
    { Déclarations publiques }
  end;

var
  DbgForm: TDbgForm;

implementation

uses MDIEdit, Main, Version;

{$R *.DFM}

procedure TDbgForm.ContinueBtnClick(Sender: TObject);
var IniFile : TIniFile;
    F       : TextFile;
begin
  IniFileName := ExtractFilePath(Application.Exename)+ ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);
  IniFile.WriteBool('Debugger','NoMsg',Msg.Checked);
  AssignFile(F,MainForm.BinDir+'gdb.bat');
  Rewrite(F);
  Writeln(F,'@echo off');
  Writeln(F,'set PATH=%PATH%;'+GetShortName(ExtractFilePath(FileName)));
  Writeln(F,'gdb '+ExtractFileName(FileName)+' -silent');
  CloseFile(F);

  ChDir(MainForm.BinDir);
  ExecuteFile(MainForm.BinDir+'gdb.bat','','', SW_SHOW);
  ChDir(MainForm.BinDir);
end;

procedure TDbgForm.FormActivate(Sender: TObject);
var IniFile : TIniFile;
begin
  IniFileName := ExtractFilePath(Application.Exename)+ ChangeFileExt(ExtractFileName(Application.Exename), '.ini');
  IniFile := TIniFile.Create(IniFilename);
  Msg.Checked := IniFile.ReadBool('Debugger','NoMsg',Msg.Checked);
end;

procedure TDbgForm.HelpBtnClick(Sender: TObject);
begin                        
  {$IFDEF CD_VERSION}
  ExecuteFile('index.html','',ExtractFilePath(Application.Exename)+'Share\gdbtcl\help\', SW_SHOW);
  {$ELSE}
  ExecuteFile('Gdb.hlp','',ExtractFilePath(Application.Exename)+'Help\', SW_SHOW);
  {$ENDIF}
end;

end.
