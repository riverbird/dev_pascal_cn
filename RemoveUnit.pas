unit RemoveUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, marsCap;

type
  TRemoveUnitForm = class(TForm)
    GroupBox: TGroupBox;
    UnitList: TListBox;
    Panel: TPanel;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  RemoveUnitForm: TRemoveUnitForm;

implementation

{$R *.DFM}

end.
