unit MsgBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, marsCap;

type
  TMessageBoxForm = class(TForm)
    Title: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    TextMemo: TMemo;
    GroupBox1: TGroupBox;
    DialogButtons: TComboBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    Image1: TImage;
    DialogIcon: TComboBox;
    DefButton: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    DialogType: TRadioGroup;
    OK: TBitBtn;
    Cancel: TBitBtn;
    Preview: TBitBtn;
    AdvGroup: TGroupBox;
    CheckBox1: TCheckBox;
    Bevel1: TBevel;
    HelpButton: TCheckBox;
    Label3: TLabel;
    RJText: TCheckBox;
    RTLRead: TCheckBox;
    Forground: TCheckBox;
    Panel2: TPanel;
    Label6: TLabel;
    TaskModal: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DialogIconChange(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure PreviewClick(Sender: TObject);
  private
    Function GetButton: String;
    Function GetButtonInt: Integer;
    Function GetCode: String;
    Function GetText: String;
    Function GetTitleText: String;
  public
    property Buttons: String read GetButton;
    property ButtonInt: Integer read GetButtonInt;
    property Code: String read GetCode;
    property Text: String read GetText;
    property TitleText: String read GetTitleText;
    { Déclarations publiques }
  end;

var
  MessageBoxForm: TMessageBoxForm;

implementation

{$R *.DFM}

Function TMessageBoxForm.GetButton: String;
var
  S: String;
begin
  S := '0';
  case DialogButtons.ItemIndex of
  0: { Nothing will happen };
  1: S := S + ' + MB_OKCANCEL';
  2: S := S + ' + MB_YESNO';
  3: S := S + ' + MB_YESNOCANCEL';
  4: S := S + ' + MB_RETRYCANCEL';
  5: S := S + ' + MB_ABORTRETRYIGNORE';
  end;

  case DefButton.ItemIndex of
  0: { Nothing will happen };
  1: S := S + ' + MB_DEFBUTTON2';
  2: S := S + ' + MB_DEFBUTTON3';
  3: S := S + ' + MB_DEFBUTTON4';
  end;

  case DialogIcon.ItemIndex of
  0: { Nothing will happen };
  1: S := S + ' + MB_ICONEXCLAMATION';
  2: S := S + ' + MB_ICONASTERISK';
  3: S := S + ' + MB_ICONQUESTION';
  4: S := S + ' + MB_ICONHAND';
  end;

  case DialogType.ItemIndex of
  0: { Nothing will happen };
  1: S := S + ' + MB_SYSTEMMODAL';
  end;

  begin
     if HelpButton.Checked then S := S + ' + MB_HELP';
     if RJText.Checked then S := S + ' + MB_RIGHT';
     if RTLRead.Checked then S := S + ' + MB_RTLREADING';

     if Forground.Checked then S := S + ' + MB_SETFOREGROUND';
     if TaskModal.Checked then S := S + ' + MB_TASKMODAL';
  end;

  Result := S;
end;

Function TMessageBoxForm.GetButtonInt: Integer;
var
  I: Integer;
begin
  I := 0;
  case DialogButtons.ItemIndex of
  0: { Nothing will happen };
  1: I := I + MB_OKCANCEL;
  2: I := I + MB_YESNO;
  3: I := I + MB_YESNOCANCEL;
  4: I := I + MB_RETRYCANCEL;
  5: I := I + MB_ABORTRETRYIGNORE;
  end;

  case DefButton.ItemIndex of
  0: { Nothing will happen };
  1: I := I + MB_DEFBUTTON2;
  2: I := I + MB_DEFBUTTON3;
  3: I := I + MB_DEFBUTTON4;
  end;

  case DialogIcon.ItemIndex of
  0: { Nothing will happen };
  1: I := I + MB_ICONEXCLAMATION;
  2: I := I + MB_ICONINFORMATION;
  3: I := I + MB_ICONQUESTION;
  4: I := I + MB_ICONHAND;
  end;

  case DialogType.ItemIndex of
  0: { Nothing will happen };
  1: I := I + MB_SYSTEMMODAL;
  end;

  if CheckBox1.Checked then
  begin
     if HelpButton.Checked then I := I + MB_HELP;
     if RJText.Checked then I := I + MB_RIGHT;
     if RTLRead.Checked then I := I + MB_RTLREADING;

     if Forground.Checked then I := I + MB_SETFOREGROUND;
     if TaskModal.Checked then I := I + MB_TASKMODAL;
  end;
  Result := I;
end;

Function TMessageBoxForm.GetCode: String;
  Function TStringsToString(Strings: TStrings): String;
  var
    i: LongInt;
    S: String;
  begin
    S := '';
    for i := 0 to Strings.Count - 1 do
    begin
       S := S + Strings.Strings[i];
       if not (i = Strings.Count - 1) then
          S := S + '\n' + #13#10;
    end;
    Result := S;
  end;
begin
  if TextMemo.Lines.Count = 0 then
     Result := ''
  else
     Result := TStringsToString(TextMemo.Lines);
end;

Function TMessageBoxForm.GetText: String;
  Function TStringsToString(Strings: TStrings): String;
  var
    i: LongInt;
    S: String;
  begin
    S := '';
    for i := 0 to Strings.Count - 1 do
    begin
       S := S + Strings.Strings[i];
       if not (i = Strings.Count - 1) then
          S := S + #13#10;
    end;
    Result := S;
  end;
begin
  if TextMemo.Lines.Count = 0 then
     Result := ' '
  else
     Result := TStringsToString(TextMemo.Lines);
end;

Function TMessageBoxForm.GetTitleText: String;
begin
  if Title.Text <> '' then
     Result := Title.Text
  else Result := ' ';
end;

procedure TMessageBoxForm.FormCreate(Sender: TObject);
begin
  DialogButtons.ItemIndex := 0;
  DefButton.ItemIndex := 0;
  DialogIcon.ItemIndex := 0;
end;

procedure TMessageBoxForm.DialogIconChange(Sender: TObject);
begin
  with Image1.Picture.Icon do
  case DialogIcon.ItemIndex of
  0: Handle := 0;
  1: Handle := LoadIcon(0, IDI_EXCLAMATION);
  2: Handle := LoadIcon(0, IDI_ASTERISK);
  3: Handle := LoadIcon(0, IDI_QUESTION);
  4: Handle := LoadIcon(0, IDI_HAND);
  end;
end;

procedure TMessageBoxForm.CheckBox1Click(Sender: TObject);
begin
  AdvGroup.Enabled := CheckBox1.Checked;
  if CheckBox1.Checked then AdvGroup.Font.Color := clWindowText
  else AdvGroup.Font.Color := clGray;
end;

procedure TMessageBoxForm.PreviewClick(Sender: TObject);
begin
  Application.MessageBox(pChar(Text), pChar(Title.Text), ButtonInt);
end;

end.
