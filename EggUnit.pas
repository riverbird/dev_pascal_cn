unit EggUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  marsCap, ExtCtrls, StdCtrls, Buttons, MPlayer;

type
  TEggForm = class(TForm)
    ColinPic: TImage;
    ColinThanks: TBitBtn;
    HongliThanks: TBitBtn;
    CloseBtn: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    HongliPic: TImage;
    ChiefPic: TImage;
    Label3: TLabel;
    ChiefThanks: TButton;
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure CloseBtnClick(Sender: TObject);
    procedure ColinThanksClick(Sender: TObject);
    procedure HongliThanksClick(Sender: TObject);
    procedure ChiefThanksClick(Sender: TObject);
  private
    Drawing : boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EggForm: TEggForm;


implementation

{$R *.DFM}

procedure TEggForm.PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Drawing := True;
  (sender as TImage).Canvas.MoveTo(X,Y);
end;

procedure TEggForm.PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Drawing := False;
end;

procedure TEggForm.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if Drawing then
     (sender as TImage).Canvas.LineTo(X,Y);
end;

procedure TEggForm.CloseBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TEggForm.ColinThanksClick(Sender: TObject);
begin
  Application.MessageBox(
  'I would like to thanks :'+#13+
  'My family, and friends : Pierre (el Pierre : ), Adeline (my sweetheart), Jessica (my weird friend ;-), C�dric, Hongli, Mumit Khan, J.J. Van der Heidjen, '+
  'Mary J., Jerome, Arnaud, Xavier (Sonia ;-p), Nicolas, Simon, Freaky Flow, '+
  'and you !','Information', MB_ICONASTERISK);
end;

procedure TEggForm.HongliThanksClick(Sender: TObject);
begin
  Application.MessageBox('My thanks list huh? Well, I would thank all ' +
    'the people who put information about Animes and Anime series on the ' +
    'Internet.' +#13#10 + 'And I would also like to thank the peoples who have created Linux. :-)', 'Information', MB_ICONASTERISK);
end;

procedure TEggForm.ChiefThanksClick(Sender: TObject);
begin
  Application.MessageBox
   ('I would like to thank my wife and children.',
   'Information',
   MB_ICONASTERISK);
end;

end.
