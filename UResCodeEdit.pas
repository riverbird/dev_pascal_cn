unit UResCodeEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtDlgs, marsCap, Buttons, ExtCtrls;

type
  TResCodeEditForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    FileName: TEdit;
    Label2: TLabel;
    ResID: TEdit;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label3: TLabel;
    LoadOption: TComboBox;
    Label4: TLabel;
    MemOption: TComboBox;
    Label5: TLabel;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Image: TImage;
    OpenDialog: TOpenDialog;
    OpenPictureDialog1: TOpenPictureDialog;
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FileNameChange(Sender: TObject);
  private
    FPictureDialog: Boolean;
    procedure SetPictureDialog(Value: Boolean);
  public
    property PictureDialog: Boolean read FPictureDialog write SetPictureDialog;
  end;

var
  ResCodeEditForm: TResCodeEditForm;

implementation

{$R *.DFM}

procedure TResCodeEditForm.SetPictureDialog(Value: Boolean);
begin
  FPictureDialog := Value;
end;

procedure TResCodeEditForm.CheckBox1Click(Sender: TObject);
begin
  Panel1.Enabled := CheckBox1.Checked;
end;

procedure TResCodeEditForm.FormCreate(Sender: TObject);
begin
  Icon.Handle := Application.Icon.Handle;
  FPictureDialog := False;
  Image.Picture.Icon.Handle := LoadIcon(0, IDI_ASTERISK);
  LoadOption.ItemIndex := 2;
end;

procedure TResCodeEditForm.BitBtn1Click(Sender: TObject);
begin
  if FileExists(FileName.Text) = False then begin
     FileName.SetFocus;
     Raise Exception.CreateFmt('The file "%s" does not exists.', [FileName.Text]);
  end;

  if ResID.Text = '' then begin
     ResID.SetFocus;
     Raise Exception.Create('You must enter a ID for this resource file.');
  end;

//  ModalResult := mrOK;
  Close;
end;

procedure TResCodeEditForm.SpeedButton1Click(Sender: TObject);
var
  Open: TOpenDialog;
begin
  if PictureDialog = True then
     Open := OpenPictureDialog1
  else
     Open := OpenDialog;

  if Open.Execute then
     FileName.Text := Open.FileName;
end;

procedure TResCodeEditForm.FileNameChange(Sender: TObject);
begin
  if (PictureDialog = True) and (FileExists(FileName.Text)) then
     Image.Picture.LoadFromFile(FileName.Text);
end;

end.
