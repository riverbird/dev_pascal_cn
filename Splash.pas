unit Splash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Version, StdCtrls;

type
  TSplashForm = class(TForm)
    Panel: TPanel;
    Image: TImage;
    StatusBar: TStatusBar;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  SplashForm: TSplashForm;

implementation

{$R *.DFM}

procedure TSplashForm.FormCreate(Sender: TObject);
begin
StatusBar.SimpleText := 'Bloodshed Dev-Pascal '+DevPasVersion+'. Loading...';
end;

end.
