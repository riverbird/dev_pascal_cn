unit MenuFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, Grids, Outline;

type
  TMenuForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ItemCaption: TEdit;
    Label2: TLabel;
    ItemID: TEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    IDMenu: TEdit;
    Button1: TButton;
    Button2: TButton;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Button3: TButton;
    Menu: TTreeView;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    ItemCount : integer;
     { Public declarations }
  end;

var
  MenuForm: TMenuForm;

implementation

{$R *.DFM}

procedure TMenuForm.Button1Click(Sender: TObject);
begin
  inc(ItemCount);

  Menu.Items.Add(nil, ItemCaption.Text);
end;

procedure TMenuForm.Button2Click(Sender: TObject);
var Node : TTreeNode;
begin
  if Menu.Selected = nil then exit;

  Node := Menu.Items.AddChild(Menu.Items[Menu.Selected.Index], ItemCaption.Text);
  Node.ImageIndex := StrToInt(ItemID.Text); // imageindex is used for storing the ID
  Menu.Selected.Expand(False);
end;

procedure TMenuForm.FormCreate(Sender: TObject);
begin
  ItemCount := 0;
end;

procedure TMenuForm.Button3Click(Sender: TObject);
begin
   if Menu.Selected <> nil then
      Menu.Items.Delete(Menu.Selected);
end;

end.
