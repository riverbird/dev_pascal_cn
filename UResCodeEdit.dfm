�
 TRESCODEEDITFORM 0�  TPF0TResCodeEditFormResCodeEditFormLeftITopYBorderIconsbiSystemMenubiHelp BorderStylebsDialogCaptionEdit resource (ResType)ClientHeight;ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBox	GroupBox1LeftTopWidth!Height� CaptionRequired options:TabOrder  TLabelLabel1LeftTop@Width� HeightCaption,&File name (only DOS 8.3 filenames allowed):FocusControlFileNameFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftToppWidth?HeightCaptionResource &ID:FocusControlResIDFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTopWidth	Height)AutoSizeCaption�Note: the filename must be a full path if the file is not in the current working directory. The path can either be a quoted or non-quoted string.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TEditFileNameLeftTopPWidthHeightTabOrder OnChangeFileNameChange TSpeedButtonSpeedButton1Left� Top�WidthHeight
Glyph.Data
�  �  BM�      B   (              P                   |  �     |                    ||||    �=�=�=�=�=�=�=�=  |||  �  �=�=�=�=�=�=�=�=  ||  ��  �=�=�=�=�=�=�=�=  |  ���                  |  ��������  ||||  ���            |||||      |||||||||||||||||||      ||||||||||||    |||||||  |||  |  ||||||||      ||||OnClickSpeedButton1Click   TEditResIDLeftTop� WidthHeightTabOrderTextMyResource1   TPanelPanel1LeftTop� Width� Heightq
BevelInnerbvRaised
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel3LeftTopWidth;HeightCaption&Load option:FocusControl
LoadOptionFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4LeftTop@WidthHHeightCaption&Memory option:FocusControl	MemOptionFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBox
LoadOptionLeftTop Width� HeightStylecsDropDownList
ItemHeightTabOrder Items.Strings( Not defined )Load immediatelyLoad on call   	TComboBox	MemOptionLeftTopPWidth� HeightStylecsDropDownList
ItemHeightTabOrderItems.Strings( Not defined )MoveableDiscardable    	TCheckBox	CheckBox1LeftTop� Width� HeightCaptionEnable optional options:TabOrderOnClickCheckBox1Click  TBitBtnBitBtn1Left8TopWidthQHeight!CaptionOKModalResultTabOrderOnClickBitBtn1Click
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn2Left8Top0WidthQHeight!Cancel	CaptionCancelModalResultTabOrder
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs  	TGroupBox	GroupBox2Left� Top� Width� HeightyCaptionPreview (not actual size):TabOrder TLabelLabel6LeftTopWidthHeightaAlignalLeftAutoSize  TLabelLabel7Left� TopWidthHeightaAlignalRightAutoSize  TLabelLabel8LeftToppWidth� HeightAlignalBottomAutoSize  TImageImageLeft
TopWidth� HeightaAlignalClientStretch	   TOpenDialog
OpenDialogFilter^True-Type Font Resources (*.ttf)|*.ttf|Bitmap-Font Resources (*.fnt)|*.fnt|All files (*.*)|*.*OptionsofEnableSizing LeftXTopX  TOpenPictureDialogOpenPictureDialog1Filter)Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.icoOptionsofEnableSizing Left8Topx   