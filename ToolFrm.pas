unit ToolFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, IniFiles;

type
  TToolForm = class(TForm)
    GroupBox1: TGroupBox;
    AddBtn: TBitBtn;
    DeleteBtn: TBitBtn;
    CloseBtn: TBitBtn;
    EditBtn: TBitBtn;
    ListBox: TListBox;
    procedure EditBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AddBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    IniFile : TIniFile;
    ToolCount : integer;
    { Public declarations }
  end;

var
  ToolForm: TToolForm;

implementation

uses ToolEditFrm;

{$R *.DFM}

procedure TToolForm.EditBtnClick(Sender: TObject);
begin
  with ToolEditForm do begin
    Title.Text := IniFile.ReadString(IntToStr(ListBox.ItemIndex+1), 'Title', '');
    ProgramEdit.Text := IniFile.ReadString(IntToStr(ListBox.ItemIndex+1), 'Program', '');
    WorkDir.Text := IniFile.ReadString(IntToStr(ListBox.ItemIndex+1), 'WorkDir', '');
    Params.Text := IniFile.ReadString(IntToStr(ListBox.ItemIndex+1), 'Params', '');
    if ShowModal = mrOK then begin
       IniFile.WriteString(IntToStr(ListBox.ItemIndex+1), 'Title', Title.Text);
       IniFile.WriteString(IntToStr(ListBox.ItemIndex+1), 'Program', ProgramEdit.Text);
       IniFile.WriteString(IntToStr(ListBox.ItemIndex+1), 'WorkDir', WorkDir.Text);
       IniFile.WriteString(IntToStr(ListBox.ItemIndex+1), 'Params', Params.Text);

       ToolForm.ListBox.Items.Insert(ListBox.ItemIndex, Title.Text);
       ToolForm.ListBox.Items.Delete(ListBox.ItemIndex);
    end;
  end;
end;

procedure TToolForm.FormActivate(Sender: TObject);
var i : integer;
begin
  IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Tools.ini');

  ToolCount := IniFile.ReadInteger('Tools','Count',0);

  for i := 1 to ToolCount do
      ListBox.Items.Add(IniFile.ReadString(IntToStr(i),'Title',''));
end;

procedure TToolForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IniFile.Free;
end;

procedure TToolForm.AddBtnClick(Sender: TObject);
begin
  with ToolEditForm do begin
    Title.Text := '';
    ProgramEdit.Text := '';
    WorkDir.Text := '';
    Params.Text := '';
    if ShowModal = mrOK then begin
       inc(ToolCount);
       IniFile.WriteString(IntToStr(ToolCount), 'Title', Title.Text);
       IniFile.WriteString(IntToStr(ToolCount), 'Program', ProgramEdit.Text);
       IniFile.WriteString(IntToStr(ToolCount), 'WorkDir', WorkDir.Text);
       IniFile.WriteString(IntToStr(ToolCount), 'Params', Params.Text);

       ToolForm.ListBox.Items.Add(Title.Text);

       IniFile.WriteInteger('Tools','Count', ToolCount);
    end;
  end;
end;

procedure TToolForm.DeleteBtnClick(Sender: TObject);
var i, k : integer;
begin
  k := ListBox.ItemIndex;
  ListBox.Items.Delete(k);

  IniFile.EraseSection(IntToStr(k+1));
  Dec(ToolCount);
  IniFile.WriteInteger('Tools','Count', ToolCount);

  for i := k+1 to ToolCount+1 do begin
      IniFile.WriteString(IntToStr(i),'Title',IniFile.ReadString(IntToStr(i+1),'Title',''));
      IniFile.WriteString(IntToStr(i),'Program',IniFile.ReadString(IntToStr(i+1),'Program',''));
      IniFile.WriteString(IntToStr(i),'WorkDir',IniFile.ReadString(IntToStr(i+1),'WorkDir',''));
      IniFile.WriteString(IntToStr(i),'Params',IniFile.ReadString(IntToStr(i+1),'Params',''));
  end;

  IniFile.EraseSection(IntToStr(ToolCount+1));

  if ListBox.Items.Count = 0 then
     EditBtn.Enabled := False;
end;

procedure TToolForm.ListBoxClick(Sender: TObject);
begin
  if ListBox.ItemIndex > -1 then
     EditBtn.Enabled := True
  else EditBtn.Enabled := False;
end;

end.
