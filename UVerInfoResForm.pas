unit UVerInfoResForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, marsCap;

type
  TVerInfoResForm = class(TForm)
    GroupBox1: TGroupBox;
    FileVersionCB: TCheckBox;
    FileVersionME1: TMaskEdit;
    FileVersionME2: TMaskEdit;
    FileVersionME3: TMaskEdit;
    FileVersionME4: TMaskEdit;
    ProductVersionCB: TCheckBox;
    ProductVersionME1: TMaskEdit;
    ProductVersionME2: TMaskEdit;
    ProductVersionME3: TMaskEdit;
    ProductVersionME4: TMaskEdit;
    procedure FileVersionCBClick(Sender: TObject);
    procedure ProductVersionCBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VerInfoResForm: TVerInfoResForm;

implementation

{$R *.DFM}




procedure TVerInfoResForm.FileVersionCBClick(Sender: TObject);
begin
  FileVersionME1.Enabled := FileVersionCB.Checked;
  FileVersionME2.Enabled := FileVersionCB.Checked;
  FileVersionME3.Enabled := FileVersionCB.Checked;
  FileVersionME4.Enabled := FileVersionCB.Checked;
end;


procedure TVerInfoResForm.ProductVersionCBClick(Sender: TObject);
begin
  ProductVersionME1.Enabled := ProductVersionCB.Checked;
  ProductVersionME2.Enabled := ProductVersionCB.Checked;
  ProductVersionME3.Enabled := ProductVersionCB.Checked;
  ProductVersionME4.Enabled := ProductVersionCB.Checked;
end;

end.
