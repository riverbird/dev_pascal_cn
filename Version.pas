unit Version;

interface
uses SysUtils, Forms, Windows;

const
      COMPLETION_FILE  = 'Completion.txt';
      DEFAULTCODE_FILE = 'Default.txt';
      ICON_DIR = 'Icon\';
      DevPasVersion = '1.9.1';

Var
      {$IFDEF FULL_VERSION}
      CompilerVersion : String = 'Free Pascal compiler v1.00';
      {$ELSE}
      CompilerVersion  : String = 'Executable version';
      {$ENDIF}

Const
      {$IFDEF CD_VERSION}
      GDBVersion = 'Cygnus Insight Debugger';
      {$ELSE}
      GDBVersion = 'GNU Debugger 4.18 (GDB)';
      {$ENDIF}

      RunBatchFileName = 'run.bat';

function GetCompiledDate : string;
function IsWinNTor2K : boolean;

implementation

function GetCompiledDate : string;
begin
  result := DateTimeToStr(FileDateToDateTime(Fileage(Application.ExeName)));
end;

function IsWinNTor2K : boolean;
var
  verInfo : TOSVERSIONINFO;
begin
  verInfo.dwOSVersionInfoSize := sizeof(verInfo);
  GetVersionEx(verInfo);
  Result := False;

  if verInfo.dwPlatformId >= VER_PLATFORM_WIN32_NT then
     Result := True;
end;

end.
