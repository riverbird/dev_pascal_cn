�
 TTOOLEDITFORM 0%
  TPF0TToolEditFormToolEditFormLeft� TopiBorderIconsbiSystemMenu BorderStylebsDialogCaption	Edit toolClientHeightfClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInchx
TextHeight TLabelLabel1Left
TopWidth HeightCaptionTitle :  TLabelLabel2Left
Top;Width:HeightCaption	Program :  TLabelLabel3Left
TopbWidthLHeightCaptionWorking Dir :  TLabelLabel4Left
Top� WidthLHeightCaptionParameters :  TLabelLabel5Left
Top� Width� HeightCaptionInsert macros in parameters :  TEditTitleLeftbTopWidthGHeightTabOrder   TEditProgramEditLeftbTop1WidthGHeightTabOrder  TEditWorkDirLeftbTopYWidthGHeightTabOrder  TEditParamsLeftbTop� WidthGHeightTabOrder  TBitBtn	CancelBtnLeftOTop@Width\HeightCaption&CancelTabOrderOnClickCancelBtnClickKindbkCancel  TBitBtnOkBtnLeft� Top@Width\HeightCaption&OKTabOrderKindbkOK  TBitBtn	BrowseBtnLeft
Top@Width\HeightCaption
&Browse...TabOrderOnClickBrowseBtnClick
Glyph.Data
�   �   BM�       v   (               �   �  �               �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUPUUUUUUUPUUUUUUUPUUUUUUUPU     UPP����PP𸸸��PP����� P��     P�����UPP�����UPP��� UPU��UUUPUp UUUPUUUUUUUPUUUUUUUP  TPanelPanel1Left
Top1Width�Height
BevelOuter	bvLoweredTabOrder  TListBox	MacroListLeft
Top� Width�HeightF
ItemHeightItems.Strings7<EXENAME> : Returns the source file or project exe name,<PROJECTNAME> : Returns the project filename(<PROJECTPATH> : Returns the project path8<CURRENTSOURCENAME> : Returns the active source filename TabOrder  TBitBtn	InsertBtnLeft
Top
WidthmHeightCaption&InsertTabOrder	OnClickInsertBtnClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  TBitBtnHelpBtnLeft� Top
Width� HeightCaption&Help on macrosTabOrder
OnClick	HelpClickKindbkHelp  TOpenDialog
OpenDialogFilter+EXE files (*.exe)|*.exe|All files (*.*)|*.*Left� Top0   